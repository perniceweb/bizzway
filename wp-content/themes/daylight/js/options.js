(function ($) {
	'use strict';

	// Template Helper Function
	$.fn.hasAttr = function(attribute) {
		var obj = this;

		if (obj.attr(attribute) !== undefined) {
			return true;
		} else {
			return false;
		}
	};

	var Base64 = {
	    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	    encode: function(e) {
	        var t = "";
	        var n, r, i, s, o, u, a;
	        var f = 0;
	        e = Base64._utf8_encode(e);
	        while (f < e.length) {
	            n = e.charCodeAt(f++);
	            r = e.charCodeAt(f++);
	            i = e.charCodeAt(f++);
	            s = n >> 2;
	            o = (n & 3) << 4 | r >> 4;
	            u = (r & 15) << 2 | i >> 6;
	            a = i & 63;
	            if (isNaN(r)) {
	                u = a = 64
	            } else if (isNaN(i)) {
	                a = 64
	            }
	            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
	        }
	        return t
	    },
	    decode: function(e) {
	        var t = "";
	        var n, r, i;
	        var s, o, u, a;
	        var f = 0;
	        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
	        while (f < e.length) {
	            s = this._keyStr.indexOf(e.charAt(f++));
	            o = this._keyStr.indexOf(e.charAt(f++));
	            u = this._keyStr.indexOf(e.charAt(f++));
	            a = this._keyStr.indexOf(e.charAt(f++));
	            n = s << 2 | o >> 4;
	            r = (o & 15) << 4 | u >> 2;
	            i = (u & 3) << 6 | a;
	            t = t + String.fromCharCode(n);
	            if (u != 64) {
	                t = t + String.fromCharCode(r)
	            }
	            if (a != 64) {
	                t = t + String.fromCharCode(i)
	            }
	        }
	        t = Base64._utf8_decode(t);
	        return t
	    },
	    _utf8_encode: function(e) {
	        e = e.replace(/rn/g, "n");
	        var t = "";
	        for (var n = 0; n < e.length; n++) {
	            var r = e.charCodeAt(n);
	            if (r < 128) {
	                t += String.fromCharCode(r)
	            } else if (r > 127 && r < 2048) {
	                t += String.fromCharCode(r >> 6 | 192);
	                t += String.fromCharCode(r & 63 | 128)
	            } else {
	                t += String.fromCharCode(r >> 12 | 224);
	                t += String.fromCharCode(r >> 6 & 63 | 128);
	                t += String.fromCharCode(r & 63 | 128)
	            }
	        }
	        return t
	    },
	    _utf8_decode: function(e) {
	        var t = "";
	        var n = 0;
	        var r = 0;
	        var c1 = 0;
	        var c2 = 0;
	        while (n < e.length) {
	            r = e.charCodeAt(n);
	            if (r < 128) {
	                t += String.fromCharCode(r);
	                n++
	            } else if (r > 191 && r < 224) {
	                c2 = e.charCodeAt(n + 1);
	                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
	                n += 2
	            } else {
	                c2 = e.charCodeAt(n + 1);
	                c3 = e.charCodeAt(n + 2);
	                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
	                n += 3
	            }
	        }
	        return t
	    }
	}

	var teslaThemes = {

		statisticsCounterVisible: false,
		progressCounter: false,

		init: function () {
			this.checkVisible();
			this.statsCounter();
			this.stickyHeader();
			this.isotopeInit();
			this.carouselInit();
			this.toggles();
			this.googleMaps();
		},

		checkVisible: function () {
			var $w = $(window);
		    $.fn.visible = function(partial,hidden,direction){

		        if (this.length < 1)
		            return;

		        var $t        = this.length > 1 ? this.eq(0) : this,
		            t         = $t.get(0),
		            vpWidth   = $w.width(),
		            vpHeight  = $w.height(),
		            direction = (direction) ? direction : 'both',
		            clientSize = hidden === true ? t.offsetWidth * t.offsetHeight : true;

		        if (typeof t.getBoundingClientRect === 'function'){

		            var rec = t.getBoundingClientRect(),
		                tViz = rec.top    >= 0 && rec.top    <  vpHeight,
		                bViz = rec.bottom >  0 && rec.bottom <= vpHeight,
		                lViz = rec.left   >= 0 && rec.left   <  vpWidth,
		                rViz = rec.right  >  0 && rec.right  <= vpWidth,
		                vVisible   = partial ? tViz || bViz : tViz && bViz,
		                hVisible   = partial ? lViz || rViz : lViz && rViz;

		            if(direction === 'both')
		                return clientSize && vVisible && hVisible;
		            else if(direction === 'vertical')
		                return clientSize && vVisible;
		            else if(direction === 'horizontal')
		                return clientSize && hVisible;
		        } else {

		            var viewTop         = $w.scrollTop(),
		                viewBottom      = viewTop + vpHeight,
		                viewLeft        = $w.scrollLeft(),
		                viewRight       = viewLeft + vpWidth,
		                offset          = $t.offset(),
		                _top            = offset.top,
		                _bottom         = _top + $t.height(),
		                _left           = offset.left,
		                _right          = _left + $t.width(),
		                compareTop      = partial === true ? _bottom : _top,
		                compareBottom   = partial === true ? _top : _bottom,
		                compareLeft     = partial === true ? _right : _left,
		                compareRight    = partial === true ? _left : _right;

		            if(direction === 'both')
		                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop)) && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
		            else if(direction === 'vertical')
		                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop));
		            else if(direction === 'horizontal')
		                return !!clientSize && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
		        }
		    };
		},

		statsCounter: function () {
			function runStats() {
				if ($('.stats-item').visible( true ) && !teslaThemes.statisticsCounterVisible) {
	            	teslaThemes.statisticsCounterVisible = true;
					$('.stats-item .number').each(function () {
					 	var $this = $(this);
					  	jQuery({ Counter: 0 }).animate({ Counter: parseInt($this.html(), 10) }, {
					    	duration: 3000,
					    	easing: 'swing',
					    	step: function (now) {
					      		$this.text(Math.ceil(now));
					    	}
					  	});
					});
				}
			
				if ($('.progress-item').visible( true ) && !teslaThemes.progressCounter) {
					teslaThemes.progressCounter = true;
					$('.progress-item .number').each(function () {
					 	var $this = $(this);
					  	jQuery({ Counter: 0 }).animate({ Counter: parseInt($this.html(), 10) }, {
					    	duration: 3000,
					    	easing: 'swing',
					    	step: function (now) {
					      		$this.text(Math.ceil(now));
					      		$this.parent('.heading').parent('.progress-bar').width(Math.ceil(now) + '%');
					    	}
					  	});
					});
				}	
			}

			runStats();

			$(document).scroll(function () {
	            runStats();
			});
		},


		stickyHeader: function () {
			if ($('header').hasClass('sticky')) {
				$(window).on('scroll', function () {
					var st = $(this).scrollTop();

					if (st > $('header').outerHeight(true) + 70) {
						$('header').addClass('fixed');
					} else {
						$('header').removeClass('fixed');
					}
				});
			}
		},

		isotopeInit: function () {
		    var isotopeContainer = $('.isotope-container');
			isotopeContainer.each(function(){
				var obj = $(this);
				var defaultSelection = obj.attr('data-default-selection');

				obj.imagesLoaded(function () {
					obj.isotope({
						filter: defaultSelection,
						itemSelector: '.isotope-item',
					    hiddenStyle: {
					      opacity: 0,
					      transform: 'scale(0.001)'
					    },
					    visibleStyle: {
					      opacity: 1,
					      transform: 'scale(1)'
					    },
					    transitionDuration: '0.8s'
					});
				});
			});

			$('.isotope-filters a').on('click', function () {
				$('.isotope-filters .current').removeClass('current');
				$(this).addClass('current');

				var selector = $(this).attr('data-filter');
					isotopeContainer.isotope({
						filter: selector
					});
				return false;
			});
		},

		carouselInit: function () {
			var i = $(".slick-carousel");

            i.each(function() {
                var e = $(this),
                    a = e.find(".carousel-items");
                a.slick({
                    focusOnSelect: !0,
                    speed: e.hasAttr("data-speed") ? e.data("speed") : 600,
                    slidesToShow: e.hasAttr("data-items-desktop") ? e.data("items-desktop") : 4,
                    arrows: e.hasAttr("data-arrows") ? e.data("arrows") : !0,
                    appendArrows: e, 
                    dots: e.hasAttr("data-dots") ? e.data("dots") : !0,
                    infinite: e.hasAttr("data-infinite") ? e.data("infinite") : !1,
                    slidesToScroll: e.hasAttr("data-items-to-slide") ? e.data("items-to-slide") : 1,
                    initialSlide: e.hasAttr("data-start") ? e.data("start") : 0,
                    asNavFor: e.hasAttr("data-as-nav-for") ? e.data("as-nav-for") : "",
                    centerMode: e.hasAttr("data-center-mode") ? e.data("center-mode") : "",
                    fade: e.hasAttr("data-fade") ? e.data("fade") : false,
                    easing: e.hasAttr("data-easing") ? e.data("easing") : "linear",
                    responsive: [
					    {
					      breakpoint: 992,
					      settings: {
					        slidesToShow: e.hasAttr("data-items-desktop") ? e.data("items-tablet") : 2,
					        slidesToScroll: e.hasAttr("data-items-desktop") ? e.data("items-tablet") : 2,
					        infinite: true,
					      }
					    },
					    {
					      breakpoint: 768,
					      settings: {
					        slidesToShow: e.hasAttr("data-items-desktop") ? e.data("items-mobile") : 1,
					        slidesToScroll: e.hasAttr("data-items-desktop") ? e.data("items-mobile") : 1
					      }
					    },
					    {
					      breakpoint: 620,
					      settings: {
					        slidesToShow: e.hasAttr("data-items-desktop") ? e.data("items-xs-mobile") : 1,
					        slidesToScroll: e.hasAttr("data-items-desktop") ? e.data("items-xs-mobile") : 1
					      }
					    }
					]
                })
            })
		},

		toggles: function () {
			// Video Popup
			$('.video-toggle').on('click', function () {
				var video_embed = decodeURIComponent(Base64.decode($(this).data('embed')));
				$('.video-popup .responsive-media').html(video_embed);
				$('html').addClass('video-popup-visible');
				return false;
			});

			$('.video-popup .media-wrapper').on('click', function (e) {
				e.stopPropagation();
			});

			$('html, .close-video-popup-toggle').on('click', function () {
				$('html').removeClass('video-popup-visible');
				var media = $('.media-wrapper');
				media.html(media.html());
			});


			// Mobile Nav Toggle
			$('.mobile-toggle').on('click', function () {
				$('body').toggleClass('mobile-nav-visibile');
				return false;
			});

			$('header .main-nav').on('click', function (e) {
				if ($(window).width() < 992) {
					e.stopPropagation();
				}
			});

			$(document).on('click', function () {
				$('body').removeClass('mobile-nav-visibile');
			});

			// Mobile Submenus

			$('header .main-nav li.menu-item-has-children > a').on('click', function (e) {
				var obj = $(this);

				if ($(window).width() < 992) {
					e.preventDefault();
					obj.parent().toggleClass('active');
					obj.next().slideToggle(250);
				}
			});

			// Post Likes
			$('div .likes').on('click', function(){
		        var box = $(this);
		        var post_id = box.data('id');
		        var likes = parseInt(box.text(),10);
		        
		        if(box.hasClass('liked')) return;
		        
		        $.ajax({
		                url: ajaxurl,
		                type: 'POST',
		                data: {action: 'post_likes', postid:post_id},
		        })
		        .done(function(result) {
		                if(result == true) {
		                    likes++;
		                    box.text(likes);
		                    box.addClass('liked')
		                }
		        })
		        .fail(function() {
		                console.log("error-ajax");
		        });
		    });
		},

		googleMaps: function () {
			function initialize_one() {
		        var mapCanvas = document.getElementsByClassName('map-canvas');
		        var Options = mapCanvas[0].getAttribute('data-map-settings');
		        var Pin = mapCanvas[0].getAttribute('data-pin');
		        var defaultSettings = JSON.parse(atob(Options));
		        var map;

		        var mapOptions = {
		            center: defaultSettings.location,
		            zoom: defaultSettings.zoom,
		            styles: [{ stylers: [{saturation: 20 }]}],
		            scrollwheel: false,
		            mapTypeId: google.maps.MapTypeId.ROADMAP
		        };

		        map = new google.maps.Map(mapCanvas[0], mapOptions);
		        var markers = [];
		        if(defaultSettings.markers) {
		            jQuery.each(defaultSettings.markers, function (i, val) {
		                var pos = new google.maps.LatLng(val.lat, val.lng);
		                markers[i] = new google.maps.Marker({
		                    position: pos,
		                    map: map,
		                    icon: Pin,
		                    animation: google.maps.Animation.DROP,
		                });
		            });
		        }
		        
		    }
		    
		    if(jQuery('.map-canvas').length) {
		        google.maps.event.addDomListener(window, 'load', initialize_one);
		    }
		},
	};

	jQuery(".vc_tta-tab").on("click", function(){
	  setTimeout(function(){
	          jQuery(".vc_tta-panel.vc_active .isotope-container").isotope('layout');
	       }, 200)
	});

	$(document).ready(function(){
		teslaThemes.init();

		$('.embed-video').each(function(){
			var iframe = '<iframe allowfullscreen src="'+$(this).data('url')+'" frame-border"0"></iframe>';
			$(this).append(iframe);
		});

		setTimeout(function(){
			$('body').addClass('dom-ready');
		}, 600);
	});
}(jQuery));