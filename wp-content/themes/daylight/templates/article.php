<?php $post_id = get_the_ID();?>
<div <?php post_class('article'); ?>>
	<?php if(has_post_thumbnail()):?>
    <div class="cover">
        <a href="<?php the_permalink();?>">
            <?php the_post_thumbnail('featured-image');?>
            <span class="post-date"><?php the_time(get_option('date_format'));?></span>
        </a>
    </div>
	<?php endif;?>

    <h2 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
    <?php if(!has_post_thumbnail()):?>
        <span class="post-date"><?php the_time(get_option('date_format'));?></span>
    <?php endif;?>
    <p class="post-description"><?php print tt_excerpt(get_the_ID(), _go('excerpt_length') ? _go('excerpt_length') : 320);?></p>

    <div class="post-meta">
        <span class="comments"><?php comments_number( esc_html__('0','daylight'), esc_html__('1','daylight'), '%'); ?></span>
        <span class="likes <?php echo isset($_COOKIE['post_likes_'. $post_id]) ? esc_attr('liked') : ''; ?>" data-id="<?php echo esc_attr($post_id);?>"><?php print get_post_meta($post_id, 'post_likes', true) ? get_post_meta($post_id, 'post_likes', true) : '0' ;?></span>
        <a class="read-more" href="<?php the_permalink();?>"><?php esc_html_e('read more','daylight');?></a>
    </div>
</div>