<?php get_header(); ?>  

<?php
/**
 * Index Page
 */

?>

<section class="hero-section page-title">
    <div class="container">
        <h1 class="hero-title"><?php esc_html_e('Blog ','daylight'); ?></h1>
        <?php echo daylight_breadcrumbs();?>
    </ul>
</section>

<section class="articles-section">
    <div class="container">
        <div class="row no-margin">
            <div class="col-md-8 articles-list">
                <?php if (have_posts()): ?>
                        <?php while(have_posts()): the_post(); 
                            get_template_part('templates/article'); 
                        endwhile; ?>
                    <?php get_template_part('templates/nav','main'); ?>
                <?php else: ?>
                    <h2><?php _e('No matching posts found','daylight'); ?></h2>
                <?php endif; ?>
            </div>

            <div class="col-md-4 no-padding">
                <?php get_sidebar();?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>