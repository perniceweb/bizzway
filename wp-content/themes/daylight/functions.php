<?php 
/***********************************************************************************************
* Tesla Framework mini branch ---  back compatibility functions used when plugin not active
* Main FW in plugin version now
/***********************************************************************************************/
require_once(get_template_directory() . '/tesla_framework/tesla.php');

if(is_admin())
    require_once( get_template_directory() . '/plugins/tgm-plugin-activation/register-plugins.php' );

add_action('tt_fw_init','tt_theme_after_fw_init');
function tt_theme_after_fw_init(){
    /***********************************************************************************************/
    /* Google fonts + Fonts changer */
    /***********************************************************************************************/
    TT_ENQUEUE::$gfont_changer = array(
        _go('global_typo_font'),
        _go('links_settings_font'),
        _go('logo_text_font'),
        _go('headings_settings_font')
    );

     TT_ENQUEUE::$base_gfonts = array('Open Sans:400,400italic,600,600italic,700,700italic,300italic,300');
}

function tt_daylight_to_js() {
    $send_js = array(
        'dirUri' => get_template_directory_uri()
    );
    wp_localize_script( 'options.js', 'themeOptions', $send_js );
    wp_register_script( 'g-map'
        , '//maps.googleapis.com/maps/api/js?key='._go('google_api_key').'&libraries=places'
        , array('jquery'), null, true );
}
add_action( 'wp_enqueue_scripts', 'tt_daylight_to_js', 12 );

if ( is_admin() ):

function tt_daylight_admin_js( $hook ) {
    wp_enqueue_script( 'admin-g-map'
        , '//maps.googleapis.com/maps/api/js?key='._go('google_api_key').'&libraries=places'
        , array('jquery'), null, true );
}
add_action( 'admin_enqueue_scripts', 'tt_daylight_admin_js', 10, 1);

endif;

/***********************************************************************************************/
/* Custom CSS */
/***********************************************************************************************/
add_action('wp_enqueue_scripts', 'tt_daylight_custom_css', 99);
function tt_daylight_custom_css() {
    $custom_css = _go('custom_css') ? _go('custom_css') : '';
    wp_add_inline_style('tt-main-style', $custom_css);
}

/***********************************************************************************************/
/* Custom JS */
/***********************************************************************************************/
add_action('wp_enqueue_scripts', 'tt_daylight_custom_js', 99, '', true);
function tt_daylight_custom_js() {
    $custom_js = _go('custom_js') ? _go('custom_js') : '';

    if(function_exists('wp_add_inline_script'))
        wp_add_inline_script('tt-custom-js', $custom_js);
}

/***********************************************************************************************/
/* Add Theme Support */
/***********************************************************************************************/
function tt_daylight_slug_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'tt_daylight_slug_setup' );

function tt_daylight_theme_favicon() {
    if( function_exists( 'wp_site_icon' ) && has_site_icon() ) {
        wp_site_icon();
    } else if(_go( 'favicon_link')){
        echo "\r\n" . sprintf( '<link rel="shortcut icon" href="%s">', _go( 'favicon_link') ) . "\r\n";
    }
}

add_action( 'wp_head', 'tt_daylight_theme_favicon');


/***********************************************************************************************/
/* Add Menus */
/***********************************************************************************************/

function tt_register_menus($return = false){
    $tt_menus = array(
            'main_menu'    => esc_html_x('Main Menu', 'dashboard','daylight'),
        );
    if($return)
        return $tt_menus;
    register_nav_menus($tt_menus);
}
add_action('init', 'tt_register_menus');


/***********************************************************************************************/
/* Custom Functions */
/***********************************************************************************************/
function tt_excerpt( $id, $length = NULL ) {
    $length = !empty($length) ? $length : 55;
    $content = apply_filters( 'the_content', get_post_field('post_content', $id));
    $content = strip_shortcodes($content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = strip_tags($content);
    $content = substr($content, 0, $length);
    return $content.'...';
}

function tt_load_composer() {
    if (class_exists('Vc_Manager')) {
        vc_set_shortcodes_templates_dir(TEMPLATEPATH . '/theme_config/vc_shortcodes');
        require_once(TEMPLATEPATH . '/theme_config/vc_shortcodes/theme_shortcodes.php');
        require_once(TEMPLATEPATH . '/theme_config/vc_shortcodes/rewrite_map.php');
        include_once(TEMPLATEPATH . '/theme_config/vc_shortcodes/params/google_map.php' );
    }
}
add_action('init','tt_load_composer',13);

function tt_extract_shortcode( $shortcode ) {
    $posts = get_posts(array(
        'posts_per_page'   => -1,
        'post_type'        => 'page',
        'post_status'      => 'publish',
    ));

    foreach($posts as $post)
        if( has_shortcode( get_post_field('post_content', $post->ID), $shortcode ) )
            return $post->ID;
}

function tt_daylight_get_page_id($shop = false){
    global $wp_query;
    $page_id = get_the_ID();
    if(is_post_type_archive('post') || is_archive() || is_search())
        return get_option('page_for_posts');

    if(tesla_has_woocommerce())
        if(is_woocommerce())
            return get_option( 'woocommerce_shop_page_id' );

    if(get_query_var('page_id'))
        $page_id = get_query_var('page_id');
    elseif(!empty($wp_query->queried_object) && !empty($wp_query->queried_object->ID))
        $page_id = $wp_query->queried_object->ID;
    elseif($shop)
        $page_id = get_option( 'woocommerce_shop_page_id' );
    else
        $page_id = false;
    return $page_id;
}

function daylight_breadcrumbs( $classes = NULL ) {
    global $post;

    if(!_go('hide_breadcrumbs')):
        $links = array();
        
        if(is_home()) {
            $page_id = get_option( 'page_for_posts' );
            $links[] = get_the_title($page_id); 

        } else if(tesla_has_woocommerce()) {
            if(is_shop()) {
                $page_id = get_option( 'woocommerce_shop_page_id' );
                $links[] = get_the_title($page_id);  
            } else if(is_product()) {
                $page_id = get_option( 'woocommerce_shop_page_id' );
                ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
                $links[] = get_the_title();
            } else if(is_product_category() || is_product_tag()) {
                $page_id = get_option( 'woocommerce_shop_page_id' );
                ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
                $links[] = get_the_archive_title();
            } 

        } else if(is_singular('post')) {
            $page_id = get_option( 'page_for_posts' );
            ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
            $links[] = get_the_title();
        } else if(is_singular('tt_portfolio')) {
            $page_id = '';
            $links[] = get_the_title();
        } else if(is_tag()) {
            $page_id = get_option( 'page_for_posts' );
            ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
            $links[] = single_tag_title('', false); 
        } else if(is_category()) {
            $page_id = get_option( 'page_for_posts' );
            ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
            $links[] = single_cat_title("", false);
        } else if(is_archive()) {
            $page_id = get_option( 'page_for_posts' );
            ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
            $links[] = get_the_archive_title();
        } else if(is_search()) {
            $page_id = get_option( 'page_for_posts' );
            $links[] = esc_html__('Search', 'daylight');
        } else if(is_front_page()) {
            
        } else if(is_page()) {
            if($post->post_parent > 0) {
                $page_id = $post->post_parent;
                if($page_id !== (int) get_option('page_on_front')) {
                    $links[get_permalink($page_id)] = get_the_title($page_id);                    
                }
            }
            $links[] = get_the_title();
        }

        ob_start(); ?>
        <ul class="tt-breadcrumbs <?php echo esc_attr($classes);?>">
            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo get_option('page_on_front') ? get_the_title(get_option('page_on_front')) : esc_html__('Home','daylight');?></a></li>
            <?php foreach ($links as $key => $link) {
                if(is_string($key)) {
                    echo sprintf('<li><a href="%s">%s</a></li>', esc_url($key), esc_html($link));
                } else {
                    echo sprintf('<li>%s</li>', esc_html($link));                               
                }
            } ?>
        </ul>
        <?php return ob_get_clean();
    endif;
}
/***********************************************************************************************/
/* Custom Colors */
/***********************************************************************************************/
add_action('wp_enqueue_scripts', 'tt_theme_custom_css', 99);

function tt_theme_custom_css() {
    $page_id = tt_get_page_id();
    $blog_id = get_option( 'page_for_posts' );

    if(is_home() || is_tag() || is_category() || is_archive() || is_search()) {
        $hero_bg = get_post_meta( $blog_id, THEME_NAME . '_page_background', true );
    } elseif (is_single()) {
        $hero_bg = get_post_meta( $page_id, THEME_NAME . '_page_background', true ) ? get_post_meta( $page_id, THEME_NAME . '_page_background', true ) : get_post_meta( $blog_id, THEME_NAME . '_page_background', true );
    } else {
        $hero_bg = get_post_meta( $page_id, THEME_NAME . '_page_background', true );
    }

    $custom_css = !empty($hero_bg['url']) ? ".hero-section {
        background: url('".$hero_bg['url']."');
    }" : '';

    $custom_css .= _go('error_background') ? ".error-404 {
        background: url('"._go('error_background')."') no-repeat top center;
    }" : '';

    $custom_css .= ( _go( 'layout_style' ) == 'Boxed' ) ? 'body {max-width: 1700px; margin: 0 auto;} ' : '';
    $custom_css .= ( _go( 'body_background' ) || ( _go( 'body_color' ) || _go( 'body_background_repeat' ) || _go( 'body_background_position' ))) ? 'body {' : '';
    $custom_css .= ( _go( 'body_background')) ? 'background-image: url('._go('body_background').'); ' : '';
    $custom_css .= ( _go( 'body_color')) ? 'background-color: '._go('body_color').'; ' : '';
    $custom_css .= ( _go( 'body_background') && _go('body_background_repeat')) ? 'background-repeat: '.strtolower(_go('body_background_repeat')).'; ' : '';
    $custom_css .= ( _go( 'body_background') && _go('body_background_position')) ? 'background-attachment: '.strtolower(_go('body_background_position')).'; ' : '';
    $custom_css .= ( _go( 'body_background') || (_go('body_color') || _go('body_background_repeat') || _go('body_background_position'))) ? '}' : '';
    
    // Main styles switches
    $custom_css .= (_go('header_bg')) ? '
        header {
            background-color: '._go('header_bg').' !important;
        }

        @media (min-width: 992px) {
            header .main-nav ul li ul {
                background-color: '.adjustBrightness(_go('header_bg'), -30).'; 
            }

            header .main-nav > ul > li:hover, header .main-nav ul li.current-menu-item {
                background: '.adjustBrightness(_go('header_bg'), -30).'; 
            }
        }

        @media (max-width: 992px) {
            header .main-nav {
                background: '.hex2rgba(_go('header_bg'), 0.85).';
            }
        }
    ' : '';

     $custom_css .= (_go('header_text')) ? '
        @media (min-width: 992px) {
            header .main-nav ul > li:hover > a, header .main-nav ul li.current-menu-item > a,
            header .main-nav ul li,
            header .main-nav ul li ul li a  {
                color: '._go('header_text').';
            }
        }

        @media (max-width: 992px) {
            header .main-nav ul li,
            header .mobile-toggle {
                color: '._go('header_text').';
            }
        }
    ' : '';

    $custom_css .= (_go('footer_color')) ? '
        footer {
            background: '._go('footer_color').';
        }
    ' : '';

    $custom_css .= (_go('footer_text')) ? '
        footer,
        footer .widget .widget-title,
        .widget.social-widget ul li a,
        .widget.social-widget ul li a:hover i,
        .widget p {
            color: '._go('footer_text').' !important;
        }
    ' : '';

    $custom_css .= (_go('primary_color')) ? '
        @media (max-width: 992px) {
             header .main-nav ul li a:hover {
                color: '._go('primary_color').' !important;
            }
        }

        .btn.orange,
        .btn.purple {
            color: '._go('primary_color').';
            border-color: '._go('primary_color').';
        }

        .btn.orange:hover,
        .subscribe-form .form-submit,
        .btn.purple:hover {
            background: '._go('primary_color').';
        }

        .search-form .form-input:focus,
        .respond-form .form-input:focus,
        .comment-form .form-input:focus,
        .subscribe-form .form-input:focus {
            border-color: '._go('primary_color').';
        }

        .subscribe-form .result_container,
        .blog-post-item .title a:hover,
        .blog-post-item .post-meta .likes.liked,
        .blog-post-item .post-meta .likes.liked:before,
        .blog-post-item .post-meta .likes:hover,
        .blog-post-item .post-meta .likes:hover:before,
        .blog-post-item .post-meta .read-more:hover,
        .article .post-meta .likes.liked,
        .article .post-meta .likes.liked:before,
        .article .post-meta .likes:hover,
        .article .post-meta .likes:hover:before,
        .article .post-footer .likes.liked,
        .article .post-footer .likes.liked:before,
        .widget ul li a:hover,
        .sidebar .widget.widget_categories ul li a:hover,
        .sidebar .widget.widget_archive ul li a:hover,
        .sidebar .widget.widget_tag_cloud .tagcloud a:hover,
        .widget.widget_latest_posts ul li .title:hover,
        .article.single .tags span,
        .article.single .tags:before,
        .article.single .tags a:hover,
        .article.single .categories a:hover,
        .article .post-title a:hover,
        .article .post-meta .read-more:hover,
        .single-project .project-meta .meta li span,
        .single-project .tags span,
        .single-project .tags:before,
        .service-item.v2 .title,
        .features-section .purchase-project h3 {
            color: '._go('primary_color').';
        }
    ' : '';

    $custom_css .= ( _go('global_typo_color' ) ) ? sprintf('body {color: %s;}', _go('global_typo_color')) : '';
    $custom_css .= ( _go('global_typo_size' ) ) ? sprintf('body {font-size: %spx;}', _go('global_typo_size')) : '';
    $custom_css .= ( _go('global_typo_font' ) ) ? sprintf('body {font-family: %s;}', _go('global_typo_font')) : '';

    $custom_css .= ( _go('links_settings_color' ) ) ? sprintf('a {color: %s;}', _go('links_settings_color')) : '';
    $custom_css .= ( _go('links_settings_size' ) ) ? sprintf('a {font-size: %spx;}', _go('links_settings_size')) : '';
    $custom_css .= ( _go('links_settings_font' ) ) ? sprintf('a {font-family: %s;}', _go('links_settings_font')) : '';

    $custom_css .= ( _go('headings_settings_color' ) ) ? sprintf('h1, h2, h3, h4, h5, h6 {color: %s;}', _go('headings_settings_color')) : '';
    $custom_css .= ( _go('headings_settings_font' ) ) ? sprintf('h1, h2, h3, h4, h5, h6 {font-family: %s;}', _go('headings_settings_font')) : '';
    
    $custom_css .= ( _go('headings_one_settings_size' ) ) ? sprintf( 'h1 {font-size: %spx;}', _go( 'headings_one_settings_size' ) ) : '';
    $custom_css .= ( _go('headings_two_settings_size' ) ) ? sprintf( 'h2 {font-size: %spx;}', _go( 'headings_two_settings_size' ) ) : '';
    $custom_css .= ( _go('headings_three_settings_size' ) ) ? sprintf( 'h3 {font-size: %spx;}', _go( 'headings_three_settings_size' ) ) : '';
    $custom_css .= ( _go('headings_four_settings_size' ) ) ? sprintf( 'h4 {font-size: %spx;}', _go( 'headings_four_settings_size' ) ) : '';
    $custom_css .= ( _go('headings_five_settings_size' ) ) ? sprintf( 'h5 {font-size: %spx;}', _go( 'headings_five_settings_size' ) ) : '';
    $custom_css .= ( _go('headings_six_settings_size' ) ) ? sprintf( 'h6 {font-size: %spx;}', _go(' headings_six_settings_size' ) ) : '';

    $custom_css .= ( _go('custom_css' ) ) ? _go( 'custom_css' ) : '';

    wp_add_inline_style( 'tt-main-style', $custom_css );
}

/***********************************************************************************************/
/* Comments */
/***********************************************************************************************/
 
function tt_custom_comments( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
    ?>

    <<?php print $tag ?> class="comment" id="comment-<?php comment_ID() ?>">
        <?php if ( 'div' != $args['style'] ) : ?>
            <div id="div-comment-<?php comment_ID() ?>" <?php comment_class(empty( $args['has_children'] ) ? 'comment-body' : 'comment-body parent') ?>>
        <?php endif; ?>
                <div class="avatar">
                    <?php if ($args['avatar_size'] != 0)
                            echo get_avatar( $comment, $args['avatar_size'], false,'avatar image' ); ?>
                </div>

                <h4 class="user-title"><?php echo get_comment_author_link() ?></h4>
                <span class="comment-date"><?php echo get_comment_time(get_option('date_format')) ?></span>
                <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'],'reply_text'=> esc_html__('Reply','daylight') ))) ?>

                <div class="message"><?php echo get_comment_text() ?></div>
        <?php if ( 'div' != $args['style'] ) : ?>
            </div>
        <?php endif; 

}

/***********************************************************************************************/
/* Add Sidebar Support */
/***********************************************************************************************/
function tt_register_sidebars(){
    if (function_exists('register_sidebar')) {
        register_sidebar(
            array(
                'name'           => esc_html__('Blog Sidebar', 'daylight'),
                'id'             => 'blog',
                'description'    => esc_html__('Blog Sidebar Area', 'daylight'),
                'before_widget'  => '<div class="col-md-12 col-sm-6 col-xs-12 isotope-item"><div class="widget %2$s">',
                'after_widget'   => '</div></div>',
                'before_title'   => '<h4 class="widget-title">',
                'after_title'    => '</h4>'
            )
        );

        register_sidebar(
            array(
                'name'           => esc_html__('Footer Column 1', 'daylight'),
                'id'             => 'footer_1',
                'description'    => esc_html__('Footer Widget Size 4', 'daylight'),
                'class'         => '',
                'before_widget'  => '<div class="widget %2$s">',
                'after_widget'   => '</div>',
                'before_title'   => '<h4 class="widget-title">',
                'after_title'    => '</h4>'
            )
        );

        register_sidebar(
            array(
                'name'           => esc_html__('Footer Column 2', 'daylight'),
                'id'             => 'footer_2',
                'description'    => esc_html__('Footer Widget Size 2', 'daylight'),
                'class'         => '',
                'before_widget'  => '<div class="widget %2$s">',
                'after_widget'   => '</div>',
                'before_title'   => '<h4 class="widget-title">',
                'after_title'    => '</h4>'
            )
        );

        register_sidebar(
            array(
                'name'           => esc_html__('Footer Column 3', 'daylight'),
                'id'             => 'footer_3',
                'description'    => esc_html__('Footer Widget Size 3', 'daylight'),
                'class'         => '',
                'before_widget'  => '<div class="widget %2$s">',
                'after_widget'   => '</div>',
                'before_title'   => '<h4 class="widget-title">',
                'after_title'    => '</h4>'
            )
        );

        register_sidebar(
            array(
                'name'           => esc_html__('Footer Column 3', 'daylight'),
                'id'             => 'footer_4',
                'description'    => esc_html__('Footer Widget Size 3', 'daylight'),
                'class'         => '',
                'before_widget'  => '<div class="widget %2$s">',
                'after_widget'   => '</div>',
                'before_title'   => '<h4 class="widget-title">',
                'after_title'    => '</h4>'
            )
        );
    }
}
add_action('widgets_init','tt_register_sidebars');


/***********************************************************************************************/
/* Share Function */
/***********************************************************************************************/
if(!function_exists('tt_share')){
    function tt_share(){
        $share_this = _go('share_this');
        if(isset($share_this) && is_array($share_this)): ?>
            <ul class="socials">
                <?php foreach($share_this as $val): ?>
                    <?php if($val === 'googleplus') $val = 'google-plus'; ?>
                        <?php switch ($val) {
                            case 'facebook': ?>
                                    <li>
                                        <a class="<?php echo esc_attr($val );?>" onClick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="fa fa-<?php echo esc_attr($val );?>"></i></a>
                                    </li>
                                <?php break; ?>
                            <?php case 'twitter': ?>
                                    <li>
                                        <a class="<?php echo esc_attr($val );?>" onClick="window.open('http://twitter.com/intent/tweet?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php the_title(); ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="fa fa-<?php echo esc_attr($val );?>"></i></a>
                                    </li>
                                <?php break; ?>
                            <?php case 'google-plus': ?>
                                    <li>
                                        <a class="<?php echo esc_attr($val );?>" onClick="window.open('https://plus.google.com/share?url=<?php echo urlencode(get_the_permalink()); ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="fa fa-<?php echo esc_attr($val );?>"></i></a>
                                    </li>
                                <?php break; ?>
                            <?php case 'pinterest': ?>
                                <li>
                                    <a class="<?php echo esc_attr($val );?>" onClick="window.open('https://www.pinterest.com/pin/create/button/?url=<?php echo urlencode(get_the_permalink()); ?>','sharer','toolbar=0,status=0,width=748,height=325');" href="javascript: void(0)"><i class="fa fa-<?php echo esc_attr($val );?>"></i></a>
                                </li>
                            <?php break; ?>
                            <?php case 'linkedin': ?>
                                    <li>
                                        <a class="<?php echo esc_attr($val );?>" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_the_permalink()); ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="fa fa-<?php echo esc_attr($val );?>"></i></a>
                                    </li>
                                    <?php break; ?>
                            <?php default: ''; break;
                        } ?>
                <?php endforeach; ?>
            </ul>
        <?php endif;
    }
}

/***********************************************************************************************/
/* View count for single posts */
/***********************************************************************************************/

function tt_set_post_views($postID) {
    $count_key = 'tt_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function tt_get_post_views($postID) {
    $count_key = 'tt_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}

/***********************************************************************************************/
/* AJAX Functions */
/***********************************************************************************************/
add_action('wp_ajax_post_likes', 'daylight_post_likes');
add_action('wp_ajax_nopriv_post_likes', 'daylight_post_likes');

function daylight_post_likes(){
    if(!empty($_POST['postid'])){
        $post_id = $_POST['postid'];
        $likes = get_post_meta($post_id, 'post_likes', true);
            if( isset($_COOKIE['post_likes_'. $post_id]) )
                die(false);
                if(!$likes)
                    $likes = 0;
                    $likes++;
                    if(update_post_meta($post_id, 'post_likes', $likes)){
                        setcookie('post_likes_'. $post_id, 'tt_'.$post_id, time()*20, '/');
                        die(true);
                    }
                    else{
                        die('error');
                    }
                }
            die();
}