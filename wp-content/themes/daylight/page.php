<?php get_header();
/**
 * Page
 */

$page_id = tt_get_page_id();
$sidebar = get_post_meta( $page_id, THEME_NAME . '_sidebar_position', true );
$hide_hero  = get_post_meta( $page_id, THEME_NAME . '_hide_hero', true );
$sidebar = $sidebar ? $sidebar : 'right'; ?>

<?php if(empty($hide_hero)):?>
<section class="hero-section page-title">
    <div class="container">
        <h1 class="hero-title"><?php echo get_the_title($page_id);?></h1>
        <?php echo daylight_breadcrumbs();?>
    </ul>
</section>
<?php endif;?>

<?php while ( have_posts() ) : the_post(); ?>
    <?php if(!has_shortcode(get_the_content(),'vc_row') || ($sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
    <section class="articles-section">
        <div class="container">
            <div class="row no-margin">
    <?php endif; ?> 
        	<?php if($sidebar == "left"): ?>   
            <div class="col-md-4 no-padding">
               <?php get_sidebar();?>
            </div>
            <?php endif;?>

            <?php if(!has_shortcode(get_the_content(),'vc_row') || ($sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
            <div class="<?php echo esc_attr( $sidebar == 'full_width' ? 'col-md-10 col-md-offset-1 full' : ($sidebar == 'left' ? 'col-md-8 left' : 'col-md-8'));?> articles-list">
                <div class="article single">
                    <h2 class="post-title"><?php the_title();?></h2>

                    <div class="description">
            <?php endif;?>

                        <?php the_content();?>

            <?php if(!has_shortcode(get_the_content(),'vc_row') || ($sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
                            
                        <?php wp_link_pages(array(
                            'next_or_number'   => 'number',
                            'nextpagelink'     => esc_html__( 'Next','daylight' ),
                            'previouspagelink' => esc_html__( 'Prev','daylight' ),
                            'pagelink'         => '%',
                            'echo' => 1
                        )); ?>

                    </div>

                    <?php comments_template();?>
                </div>
            </div>
            <?php endif;?>

            <?php if($sidebar == "right"): ?>   
            <div class="col-md-4 no-padding">
               <?php get_sidebar();?>
            </div>
            <?php endif;?>
<?php if(!has_shortcode(get_the_content(),'vc_row') || ($sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
            </div>
        </div>
    </section>
<?php endif;?>
<?php endwhile;?>

<?php get_footer();?>