<?php get_header();
/**
 * Single Portfolio
 */

$page_id = tt_get_page_id();
?>

<section class="hero-section page-title">
    <div class="container">
        <h1 class="hero-title"><?php esc_html_e('Single Project','daylight');?></h1>
        <?php echo daylight_breadcrumbs();?>
    </ul>
</section>

<section class="single-project article">
	<div class="container">
		<?php while ( have_posts() ) : the_post(); 
			$terms = get_the_terms( get_the_ID(),'tt_portfolio_tax');
			$options = get_post_meta(get_the_ID(), 'slide_options', true);
			$post_thumbnail_url = wp_get_attachment_url( $options['full_image'] );?>

		<h2 class="project-title"><?php the_title();?></h2>

		<?php if($post_thumbnail_url):?>
		<div class="project-cover">
			<img src="<?php echo esc_url($post_thumbnail_url); ?>" alt="<?php echo esc_attr(get_the_title());?>" />
		</div>
		<?php endif;?>

		<div class="row project-details">
			<div class="col-md-5">
				<div class="project-meta">
					<h4><?php esc_html_e('project details','daylight');?></h4>

					<ul class="meta">
						<?php 
							if($options['author'])
								printf('<li><span>%s</span> %s</li>', esc_html__('author:','daylight'),$options['author']);

							printf('<li><span>%s</span> %s</li>', esc_html__('date:','daylight'), get_the_time(get_option('date_format')));
							if(!is_wp_error($terms)) {
								if($terms) {
									print '<li><span>'.esc_html__('category: ','daylight').'</span>';
									foreach($terms as $term) print $term->name.', ';
									print '</li>';
								}
							} 
							if($options['client'])
								printf('<li><span>%s</span> %s</li>', esc_html__('client:','daylight'),$options['client']);
						?>
					</ul>

					<?php echo tt_share();?>
				</div>
			</div>

			<div class="col-md-7">
				<div class="description">
					<?php the_content();?>
				</div>

				<?php the_tags('<p class="tags"><span>Tags</span>', ', ', '</p>');?>
			</div>
		</div>

		<?php comments_template();?>

		<?php 
	        $related_query = '';
	        $port_tax = array();

	        if(_go('show_related')) {
	            $settings = _go('related_settings') ? explode("|", _go('related_settings')) : '';
	            $terms = wp_get_post_terms( $page_id, 'tt_portfolio_tax');
	            $cats = '';
	            foreach($terms as $term)
	            	array_push($port_tax, $term->slug);

	            $related_query = new WP_Query( array(
	                'post_type' => 'tt_portfolio',
	                'post_status' => 'publish',
	               	'tax_query' => array(
						array(
							'taxonomy' => 'tt_portfolio_tax',
							'field'    => 'slug',
							'terms'    => $port_tax,
						),
					),
	                'post__not_in' => array($page_id),
	                'showposts' => !empty($settings[1]) ? $settings[1] : 3,
	            ));
	        }
	    if(!empty($related_query) && $related_query->have_posts()): ?>
	    <div class="related-posts">
	         <h3 class="title"><?php print !empty($settings[0]) ? $settings[0] : '';?></h3>

	         <div class="row isotope-container">
	            <?php while($related_query -> have_posts()) : $related_query->the_post();
	            	$options = get_post_meta(get_the_ID(), 'slide_options', true);
					$post_thumbnail_url = wp_get_attachment_url( $options['cover_image'] );
				?>
		            <?php if($post_thumbnail_url):?>
		            <div class="col-xs-6 col-md-4 isotope-item">
						<div class="project-item">
							<a href="<?php the_permalink();?>">
								<img src="<?php echo esc_url($post_thumbnail_url); ?>" alt="<?php echo esc_attr(get_the_title());?>" />
							</a>
							<h3 class="cover-title"><?php the_title();?></h3>
						</div>
					</div>
					<?php endif;?>	
	            <?php endwhile; wp_reset_postdata();?>
	        </div>
	    </div>
	    <?php endif; ?>

		<?php endwhile;?>
	</div>
</section>

<?php get_footer();?>