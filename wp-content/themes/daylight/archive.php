<?php get_header(); ?>  

<?php
/**
 * Archives Page
 */
$page_id = get_option('page_for_posts');
$sidebar = get_post_meta( $page_id, THEME_NAME . '_sidebar_position', true );
$sidebar = $sidebar ? $sidebar : 'right';
?>

<section class="hero-section page-title">
    <div class="container">
        <h1 class="hero-title">
        	<?php if (is_category()) { ?><?php esc_html_e('Category: ', 'daylight'); ?> <?php single_cat_title(); ?>
            <?php } elseif( is_tag() ) { ?><?php esc_html_e('Post Tagged with: ', 'daylight'); ?> "<?php single_tag_title(); ?>"
            <?php } elseif (is_day()) { ?><?php esc_html_e('Archive for: ', 'daylight'); ?> <?php the_time('F jS, Y'); ?>
            <?php } elseif (is_month()) { ?><?php esc_html_e('Archive for: ', 'daylight'); ?> <?php the_time('F, Y'); ?>
            <?php } elseif (is_year()) { ?><?php esc_html_e('Archive for: ', 'daylight'); ?> <?php the_time('Y'); ?>
            <?php } elseif (is_author()) { ?><?php esc_html_e('Author Archives: ', 'daylight'); echo get_the_author(); ?>  
            <?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?><?php esc_html_e('Archives', 'daylight');?>
            <?php } ?>
        </h1>
        <?php echo daylight_breadcrumbs();?>
    </ul>
</section>

<section class="articles-section">
    <div class="container">
        <div class="row no-margin">
            <?php if($sidebar == "left"): ?> 
            <div class="col-md-4 no-padding">
                <?php get_sidebar();?>
            </div>
            <?php endif;?>

            <div class="<?php echo esc_attr( $sidebar == 'full_width' ? 'col-md-10 col-md-offset-1 full' : ($sidebar == 'left' ? 'col-md-8 left' : 'col-md-8'));?> articles-list">
                <?php if (have_posts()): ?>
                        <?php while(have_posts()): the_post(); 
                            get_template_part('templates/article'); 
                        endwhile; ?>
                    <?php get_template_part('templates/nav','main'); ?>
                <?php else: ?>
                    <h2><?php _e('No matching posts found','daylight'); ?></h2>
                <?php endif; ?>
            </div>

            <?php if($sidebar == "right"): ?> 
            <div class="col-md-4 no-padding">
                <?php get_sidebar();?>
            </div>
            <?php endif;?>
        </div>
    </div>
</section>

<?php get_footer(); ?>