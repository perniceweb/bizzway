<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

     <!-- Pingbacks -->
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<?php wp_head(); ?>
</head>
    
<body id="page" <?php body_class();?>>
    <?php 
        $page_id = tt_get_page_id(); 
        if ( _go('header_style') == "page-default" || _go('header_style') == NULL):
            $header = get_post_meta($page_id, THEME_NAME . '_header_type', true );    
        else :
           $header = _go('header_style');
        endif;
        $logo_position = _go('logo_position') ? 'align'._go('logo_position') : ''; ?>
    <div class="page-wrapper">
        <header class="clearfix <?php echo esc_attr($header);?>">
            <div class="container">
                <a href="<?php echo home_url('/'); ?>" style="<?php _estyle_changer('logo_text') ?>" class="logo-wrapper <?php echo esc_attr($logo_position);?>">
                    <?php if(_go('logo_image')): ?>
                        <img src="<?php _eo('logo_image') ?>" alt="<?php echo THEME_PRETTY_NAME ?>">
                    <?php elseif(_go('logo_text')): ?>
                        <?php _eo('logo_text') ?>
                    <?php else: ?>
                        <img src="<?php echo get_template_directory_uri().'/images/site-logo.png';?>" alt="<?php echo THEME_PRETTY_NAME ?>">
                    <?php endif; ?>
                </a>

                <nav class="main-nav <?php print $logo_position == 'alignright' ? 'alignleft' : '';?>">
                    <ul>
                        <?php wp_nav_menu( array( 
                            'title_li'=>'',
                            'theme_location' => 'main_menu',
                            'container' => false,
                            'items_wrap' => '%3$s',
                            'fallback_cb' => 'wp_list_pages'
                            ));
                        ?>
                    </ul>
                </nav>

                <span class="mobile-toggle"></span>
            </div>
        </header>

        <div class="content-wrapper">