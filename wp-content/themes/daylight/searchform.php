<form class="search-form" method="get" role="search" action="<?php echo home_url('/') ?>">
	<input class="form-input" type="text" name="s" id="s" value="" placeholder="<?php esc_attr_e('Search','daylight' );?>"  autocomplete="off">
	<button class="form-submit">
		<i class="fa fa-search"></i>
	</button>
</form>