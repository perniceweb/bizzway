<?php

class Tesla_social_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
				'tesla_social_widget',
				'['.THEME_PRETTY_NAME.'] Social',
				array(
					'description' => __('Displays social links', 'daylight'),
					'classname' => 'social-widget',
				)
		);
	}

	function widget($args, $instance) {
		extract($args);
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		print $before_widget; 
		
		if(!empty($title)){
			print $before_title . $title . $after_title;
		}	?>

		<ul>
		<?php $social_platforms = array('facebook','twitter','google','pinterest','instagram','linkedin','dribbble','behance','youtube','flickr','rss');
        foreach($social_platforms as $platform): 
            if (_go('social_platforms_' . $platform)):?>
                <li>
                    <a href="<?php echo esc_url(_go('social_platforms_' . $platform)); ?>"><i class="fa fa-<?php echo esc_attr($platform); ?>" title="<?php echo esc_attr($platform); ?>"></i><?php echo esc_attr(ucfirst($platform)); ?></a>
                </li>
            <?php endif;
        endforeach; ?>
        </ul>

        <?php print $after_widget;
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
	
		return $instance;
	}

	function form($instance) {
		$title   = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : ''; ?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:' , 'daylight'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p>
			<?php esc_html_e('This widget is using social links from framework settings','daylight');?>
		</p>
		<?php
	}
}

add_action('widgets_init', create_function('', 'return register_widget("Tesla_social_widget");'));