<?php

class Tesla_about_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
				'tesla_about_widget',
				'['.THEME_PRETTY_NAME.'] About',
				array(
					'description' => __('Displays website info', 'daylight'),
					'classname' => 'widget-about',
				)
		);
	}

	function widget($args, $instance) {
		extract($args);
		$description = !empty($instance['description']) ? $instance['description'] : '';
		$logo_url = !empty($instance['logo_url']) ? $instance['logo_url'] : '';
		$copyright = !empty($instance['copyright']) ? $instance['copyright'] : '';

		print $before_widget;
		?>
			<a href="<?php echo home_url('/'); ?>" style="<?php _estyle_changer('logo_text') ?>">
				<?php if($logo_url):?>
					<img src="<?php echo esc_url($logo_url);?>" alt="<?php echo THEME_PRETTY_NAME ?>">
				<?php else: ?>
	                <?php if(_go('logo_image')): ?>
	                    <img src="<?php _eo('logo_image') ?>" alt="<?php echo THEME_PRETTY_NAME ?>">
	                <?php elseif(_go('logo_text')): ?>
	                    <?php _eo('logo_text') ?>
	                <?php else: ?>
	                    <img src="<?php echo get_template_directory_uri().'/images/logo.png';?>" alt="<?php echo THEME_PRETTY_NAME ?>">
	                <?php endif; ?>
	            <?php endif;?>
            </a>

			<p><?php print $description;?></p>

			<p class="copyright">
				<?php print !empty($copyright) ? $copyright : esc_html__('Copyright ','daylight').date('Y').' <a href="'.esc_url('https://www.teslathemes.com/').'" target="_blank">'.esc_html__('TeslaThemes','daylight').'</a>'.', '.esc_html__('Supported by ','daylight').'<a href="'.esc_url('https://wpmatic.io/').'" target="_blank">'.esc_html__('WPmatic','daylight').'</a>'; ?>
			</p>
		<?php
		print $after_widget;
	}

	function update($new_instance, $old_instance) {
		$instance = array();
		$instance['description'] = $new_instance['description'];
		$instance['logo_url'] = $new_instance['logo_url'];
		$instance['copyright'] = $new_instance['copyright'];

		return $instance;
	}

	function form($instance) {
		empty($instance['description'])? $instance['description'] = '': $instance['description'] = $instance['description'];
		empty($instance['logo_url'])? $instance['logo_url'] = '': $instance['logo_url'] = $instance['logo_url'];
		empty($instance['copyright'])? $instance['copyright'] = '': $instance['copyright'] = $instance['copyright'];
		$description = esc_attr($instance['description']);
		$logo_url = esc_attr($instance['logo_url']);
		$copyright = esc_attr($instance['copyright']);
		?>

		<p>
			<label><?php _e('Logo URL:','daylight'); ?><input class="widefat" name="<?php echo esc_attr($this->get_field_name('logo_url')); ?>" type="text" value="<?php echo esc_attr($logo_url); ?>" /></label>
		</p>

		<p>
			<label><?php _e('Description:','daylight'); ?><input class="widefat" name="<?php echo esc_attr($this->get_field_name('description')); ?>" type="text" value="<?php echo esc_attr($description); ?>" /></label>
		</p>

		<p>
			<label><?php _e('Copyright:','daylight'); ?><input class="widefat" name="<?php echo esc_attr($this->get_field_name('copyright')); ?>" type="text" value="<?php echo esc_attr($copyright); ?>" /></label>
		</p>
		<?php
	}
}

add_action('widgets_init', create_function('', 'return register_widget("Tesla_about_widget");'));