<?php get_header();
/**
 * Single Post
 */

$page_id = tt_get_page_id();
$sidebar_option = get_post_meta( $page_id, THEME_NAME . '_sidebar_position', true );

switch ($sidebar_option) {
	case 'as_blog':
		$s_id = get_option( 'page_for_posts' );	
		break;
	case 'full_width':
		$s_id = $page_id;
		break;
	case 'right':
		$s_id = $page_id;
		break;
	case 'left':
		$s_id = $page_id;
}

if(!empty($s_id))
	$sidebar = get_post_meta( $s_id, THEME_NAME . '_sidebar_position', true );
	
$sidebar = empty($sidebar) ? 'right' : $sidebar;
$hide_hero  = get_post_meta( $page_id, THEME_NAME . '_hide_hero', true ); ?>

<?php if(empty($hide_hero)):?>
<section class="hero-section page-title">
    <div class="container">
        <h1 class="hero-title"><?php echo get_the_title(get_option('page_for_posts'));?></h1>
        <?php echo daylight_breadcrumbs();?>
    </ul>
</section>
<?php endif;?>

<?php while ( have_posts() ) : the_post(); ?>
    <?php if(!has_shortcode(get_the_content(),'vc_row') || ($sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
    <section class="articles-section">
        <div class="container">
            <div class="row no-margin">
    <?php endif; ?> 
        	<?php if($sidebar == "left"): ?>   
            <div class="col-md-4 no-padding">
               <?php get_sidebar();?>
            </div>
            <?php endif;?>

            <?php if(!has_shortcode(get_the_content(),'vc_row') || ($sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
            <div class="<?php echo esc_attr( $sidebar == 'full_width' ? 'col-md-10 col-md-offset-1 full' : ($sidebar == 'left' ? 'col-md-8 left' : 'col-md-8'));?> articles-list">
                <div class="article single">
                	<?php if(has_post_thumbnail()):?>
                	<div class="cover aligncenter">
						<?php the_post_thumbnail();?>
						<span class="post-date"><?php the_time(get_option('date_format'));?></span>
					</div>
					<?php endif;?>

                    <h2 class="post-title"><?php the_title();?></h2>

                    <div class="categories">
                    	<?php esc_html_e('Category: ','daylight');?><?php the_category(', ');?>
                    </div>
                    <div class="description">
            <?php endif;?>

                        <?php the_content();?>

            <?php if(!has_shortcode(get_the_content(),'vc_row') || ($sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
                            
                        <?php wp_link_pages(array(
                            'next_or_number'   => 'number',
                            'nextpagelink'     => esc_html__( 'Next','daylight' ),
                            'previouspagelink' => esc_html__( 'Prev','daylight' ),
                            'pagelink'         => '%',
                            'echo' => 1
                        )); ?>

                    </div>

                    <?php the_tags('<div class="tags"><span>Tags</span>', ', ', '</div>');?>

                    <div class="post-footer clearfix">
						<?php print tt_share();?>

                    	<span class="likes <?php echo isset($_COOKIE['post_likes_'. $page_id]) ? esc_attr('liked') : ''; ?>" data-id="<?php echo esc_attr($page_id);?>"><?php print get_post_meta($page_id, 'post_likes', true) ? get_post_meta($page_id, 'post_likes', true) : '0' ;?></span>
					</div>

                    <?php comments_template();?>
                </div>
            </div>
            <?php endif;?>

            <?php if($sidebar == "right"): ?>   
            <div class="col-md-4 no-padding">
               <?php get_sidebar();?>
            </div>
            <?php endif;?>
<?php if(!has_shortcode(get_the_content(),'vc_row') || ($sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
            </div>
        </div>
    </section>
<?php endif;?>
<?php endwhile;?>

<?php get_footer();?>