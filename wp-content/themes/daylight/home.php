<?php get_header(); ?>  

<?php
/**
 * Blog Page
 */
$page_id = tt_get_page_id();
$sidebar = get_post_meta( $page_id, THEME_NAME . '_sidebar_position', true );
$sidebar = $sidebar ? $sidebar : 'right';
?>

<section class="hero-section page-title">
    <div class="container">
        <h1 class="hero-title"><?php esc_html_e('Blog ','daylight'); ?></h1>
        <?php echo daylight_breadcrumbs();?>
    </ul>
</section>

<section class="articles-section">
    <div class="container">
        <div class="row no-margin">
            <?php if($sidebar == "left"): ?> 
            <div class="col-md-4 no-padding">
                <?php get_sidebar();?>
            </div>
            <?php endif;?>

            <div class="<?php echo esc_attr( $sidebar == 'full_width' ? 'col-md-10 col-md-offset-1 full' : ($sidebar == 'left' ? 'col-md-8 left' : 'col-md-8'));?> articles-list">
                <?php if (have_posts()): ?>
                        <?php while(have_posts()): the_post(); 
                            get_template_part('templates/article'); 
                        endwhile; ?>
                    <?php get_template_part('templates/nav','main'); ?>
                <?php else: ?>
                    <h2><?php _e('No matching posts found','daylight'); ?></h2>
                <?php endif; ?>
            </div>

            <?php if($sidebar == "right"): ?> 
            <div class="col-md-4 no-padding">
                <?php get_sidebar();?>
            </div>
            <?php endif;?>
        </div>
    </div>
</section>

<?php get_footer(); ?>