<div class="row">
	<?php if( is_active_sidebar( 'footer_1' )):?>
		<div class="col-md-4 col-sm-6"> 
		<?php dynamic_sidebar('footer_1'); ?>
		</div>
	<?php endif;?>

	<?php if( is_active_sidebar( 'footer_2' )):?>
		<div class="col-md-2 col-sm-6"> 
		<?php dynamic_sidebar('footer_2'); ?>
		</div>
	<?php endif;?>

	<?php if( is_active_sidebar( 'footer_3' )):?>
		<div class="col-md-3 col-sm-6"> 
		<?php dynamic_sidebar('footer_3'); ?>
		</div>
	<?php endif;?>

	<?php if( is_active_sidebar( 'footer_4' )):?>
		<div class="col-md-3 col-sm-6"> 
		<?php dynamic_sidebar('footer_4'); ?>
		</div>
	<?php endif;?>
</div>
