<?php 
wp_reset_postdata();
if(comments_open() || have_comments()) : ?>
	<?php 
	$args = array(
		'fields' => apply_filters( 'comment_form_default_fields', array(
			'author' => '<input name="author" class="form-input" type="text" value="' . esc_attr( $commenter[ 'comment_author' ] ) . '" aria-required="true" placeholder="'.esc_html__('Enter your name','daylight').'">',
			'email' => '<input name="email" type="text" class="form-input" value="' . esc_attr( $commenter[ 'comment_author_email' ] ) . '" aria-required="true" placeholder="'.esc_html__('Enter your e-mail address','daylight').'">',
				)
		),

		'comment_notes_after' => '',
		'comment_notes_before' => '',
		'title_reply' => _x('LEAVE A REPLY','comment-form','daylight'),
		'comment_field' => '<textarea name="comment" class="form-input" placeholder="'.esc_html__('Message','daylight').'"></textarea>',
		'class_submit' => 'btn form-submit purple',
		'label_submit' => esc_html_x('write','comment-form','daylight')
		);

		comment_form( $args );
	?>

	<?php if ( have_comments() ) : ?>
	<div class="comments-area">
		<?php if ( post_password_required() ) : ?>
					<p><?php esc_html_e( 'This post is password protected. Enter the password to view any comments ', 'daylight' ); ?></p>
				</div>

			<?php
			/* Stop the rest of comments.php from being processed,
			 * but don't kill the script entirely -- we still have
			 * to fully load the template.
			 */
			return;
			endif;?>

		<?php if ( have_comments() ) : ?>
			<h4 class="area-title"><?php comments_number( esc_html__('No Comments','daylight'), esc_html__('Comments (1)','daylight'), esc_html__('Comments','daylight') . ' (%)'); ?></h4>

			<div class="comments_navigation page-numbers">
				<?php paginate_comments_links(array(
				'show_all'     => False,
				'end_size'     => 1,
				'mid_size'     => 2,
				'prev_next'    => True,
				'prev_text'    => '&larr;',
				'next_text'    => '&rarr;',
				'type'         => 'list',
				'add_args'     => False,
				'add_fragment' => ''
			)); ?>
			</div>

			<ul class="clean-list comments-list">
				<?php wp_list_comments( array( 'callback' => 'tt_custom_comments' , 'avatar_size'=>'54','style'=>'ul') ); ?>
			</ul>	
		
		<?php endif; ?>
	</div>
	<?php endif;?>
<?php endif; ?>