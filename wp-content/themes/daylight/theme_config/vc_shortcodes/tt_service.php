<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

vc_icon_element_fonts_enqueue( $type );
$icon = ${"icon_" . $type}; ?>

<?php if($service_type == 'type-1'):?>
<div class="service-item <?php echo esc_attr($css_class.' '.$el_class);?>">
	<i class="service-icon <?php echo esc_attr($icon);?>" <?php print $color_icon ? 'style="color: '.$color_icon.'"' : '';?>></i>
	<h3 class="title" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php print $title;?></h3>
	<p class="description" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php print $subtext;?></p>
</div>
<?php elseif($service_type == 'type-3'):?>
<div class="service-item v2 <?php echo esc_attr($css_class.' '.$el_class);?>">
	<i class="service-icon <?php echo esc_attr($icon);?>" style="<?php print $color_icon ? 'color: '.$color_icon.'; ' : ''; print $background_icon ? 'background: '.$background_icon.';' : ''; ?>"></i>
	<h3 class="title" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php print $title;?></h3>
	<p class="description" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php print $subtext;?></p>
</div>
<?php elseif($service_type == 'type-2'):?>
<div class="feature-item <?php echo esc_attr($css_class.' '.$el_class);?>">
	<i class="service-icon <?php echo esc_attr($icon);?>" style="<?php print $color_icon ? 'color: '.$color_icon.'; ' : ''; print $background_icon ? 'background: '.$background_icon.';' : ''; ?>"></i>
	<h3 class="title" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php print $title;?></h3>
	<p class="description" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php print $subtext;?></p>
</div>
<?php elseif($service_type == 'type-4'):?>
<div class="service-item v2 right <?php echo esc_attr($css_class.' '.$el_class);?>">
	<i class="service-icon <?php echo esc_attr($icon);?>" style="<?php print $color_icon ? 'color: '.$color_icon.'; ' : ''; print $background_icon ? 'background: '.$background_icon.';' : ''; ?>"></i>
	<h3 class="title" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php print $title;?></h3>
	<p class="description" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php print $subtext;?></p>
</div>
<?php endif;?>