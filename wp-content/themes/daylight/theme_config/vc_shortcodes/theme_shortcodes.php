<?php
/* TT Custom Heading */
class WPBakeryShortCode_TT_section_header extends WPBakeryShortCode {
	public function getStyles( $font_container_data ) {
		$styles = array();

		if (!empty($font_container_data)) {
			$pieces = explode("|", $font_container_data);

			foreach($pieces as $piece) 
				$styles[] = str_replace(array('_', '%23'), array('-', '#'), $piece).';';			
		}
		return implode('',$styles);
	}
}
/* TT Blog Feed */
class WPBakeryShortCode_TT_blog_list extends WPBakeryShortCode {
}

/* TT Link */
class WPBakeryShortCode_TT_link extends WPBakeryShortCode {
}

/* TT Instagram */
class WPBakeryShortCode_TT_instagram extends WPBakeryShortCode {
}

/* TT Google Map */
class WPBakeryShortCode_TT_GMap extends WPBakeryShortCode {
}

/* TT Service */
class WPBakeryShortCode_TT_service extends WPBakeryShortCode {
}
/* TT Counter */
class WPBakeryShortCode_TT_counter extends WPBakeryShortCode {
}

/* TT Subscribe Form */
class WPBakeryShortCode_TT_subscribeform extends WPBakeryShortCode {
}

/* TT Pricing */
class WPBakeryShortCode_TT_pricing extends WPBakeryShortCode {
}

/* TT ProgressBar */
class WPBakeryShortCode_TT_progressbar extends WPBakeryShortCode {
}

/* TT Breadcrumbs */
class WPBakeryShortCode_TT_breadcrumbs extends WPBakeryShortCode {
}

/* TT Breadcrumbs */
class WPBakeryShortCode_TT_video_toggle extends WPBakeryShortCode {
}






