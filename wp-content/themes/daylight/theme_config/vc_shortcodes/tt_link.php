<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$url = vc_build_link( $link );?>

<a class="<?php print $css_class.' '.$el_class;?>" href="<?php echo esc_attr($url['url']) ?>" title="<?php echo esc_attr($url['title']) ?>" target="<?php echo ( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self') ?>">
	<?php echo esc_attr($url['title']) ?>
</a>