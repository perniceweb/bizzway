<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : ''; ?>

<div class="progress-item <?php echo esc_attr($css_class.' '.$el_class);?>">
	<span class="caption"><?php print $title;?></span>
	<div class="progress-bar" <?php print $bar_bg ? 'style="background: '.$bar_bg.'"' : '' ;?>>
		<div class="heading">
			<span class="number"><?php echo esc_attr($b_width);?></span>%
		</div>
	</div>
</div>