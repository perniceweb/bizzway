<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

vc_icon_element_fonts_enqueue( $type );
$icon = ${"icon_" . $type}; ?>

<div class="stats-item <?php echo esc_attr($css_class.' '.$el_class);?>">
	<i class="service-icon <?php echo esc_attr($icon);?>" <?php print $color_icon ? 'style="color: '.$color_icon.'"' : '';?>></i>
	<span class="number" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php print $number;?></span>
	<span class="caption" <?php print $text_color ? 'style="color: '.$text_color.'"' : '' ;?>><?php print $title;?></span>
</div>