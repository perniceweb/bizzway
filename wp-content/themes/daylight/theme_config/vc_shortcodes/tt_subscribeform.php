<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : ''; ?>

<form class="subscribe-form clearfix <?php echo esc_attr($css_class.' '.$el_class);?>" method="post" data-tt-subscription>	
	<div clsss="row">
		<div class="col-sm-4">
			<input type="text" name="name" class="form-input" placeholder="<?php esc_attr_e('Enter your name','daylight');?>"  data-tt-subscription-required data-tt-subscription-type="name" />
		</div>

		<div class="col-sm-5">
			<input type="text" name="email" class="form-input" placeholder="<?php esc_attr_e('Enter your e-mail','daylight');?>"  data-tt-subscription-required data-tt-subscription-type="email" />
		</div>

		<div class="col-sm-3">
			<button type="submit" class="form-submit"><?php esc_html_e('newsletter subscribe','daylight');?></button>
		</div>
	</div>

	<div class="result_container"></div>
</form>