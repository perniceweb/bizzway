<?php

$vc_is_wp_version_3_6_more = version_compare( preg_replace( '/^([\d\.]+)(\-.*$)/', '$1', get_bloginfo( 'version' ) ), '3.6' ) >= 0;

add_filter( 'vc_iconpicker-type-icomoon', 'tt_icon');

$colors_arr = array (
	esc_html__( 'Blue', 'js_composer' ) => 'blue',
	esc_html__( 'Turquoise', 'js_composer' ) => 'turquoise',
	esc_html__( 'Pink', 'js_composer' ) => 'pink',
	esc_html__( 'Violet', 'js_composer' ) => 'violet',
	esc_html__( 'Peacoc', 'js_composer' ) => 'peacoc',
	esc_html__( 'Chino', 'js_composer' ) => 'chino',
	esc_html__( 'Mulled Wine', 'js_composer' ) => 'mulled_wine',
	esc_html__( 'Vista Blue', 'js_composer' ) => 'vista_blue',
	esc_html__( 'Black', 'js_composer' ) => 'black',
	esc_html__( 'Orange', 'js_composer' ) => 'orange',
	esc_html__( 'Sky', 'js_composer' ) => 'sky',
	esc_html__( 'Green', 'js_composer' ) => 'green',
	esc_html__( 'Juicy pink', 'js_composer' ) => 'juicy_pink',
	esc_html__( 'Sandy brown', 'js_composer' ) => 'sandy_brown',
	esc_html__( 'Purple', 'js_composer' ) => 'purple',
	esc_html__( 'White', 'js_composer' ) => 'white',
);

$size_arr = array (
	esc_html__( 'Mini', 'js_composer' ) => 'xs',
	esc_html__( 'Small', 'js_composer' ) => 'sm',
	esc_html__( 'Normal', 'js_composer' ) => 'md',
	esc_html__( 'Large', 'js_composer' ) => 'lg'
);

function tt_icon($icons) {
	$icomoon = array(
		array('icon-photo-camera2' => 'Camera'),
		array('icon-speedometer' => 'Speedometer'),
		array('icon-imac' => 'IMac'),
		array('icon-headphones' => 'Headphones'),
		array('icon-stack-of-three-coins' => 'Stack Coins'),
		array('icon-light-bulb' => 'Light Bulb'),
		array('icon-view' => 'View'),
		array('icon-settings' => 'Gear'),
		array('icon-user' => 'User'),
		array('icon-microphone' => 'Microphone'),
		array('icon-notepad' => 'Notepad'),
		array('icon-glasses' => 'Glasses'),
		array('icon-coffee-cup' => 'Coffee'),
		array('icon-star' => 'Star'),
		array('icon-fountain-pen' => 'Pen'),
	);
	return array_merge($icons, $icomoon);
}

function tt_icons() {
	$icons = array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'js_composer' ),
				'value' => array(
					esc_html__( 'Font Awesome', 'js_composer' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'js_composer' ) => 'openiconic',
					esc_html__( 'Typicons', 'js_composer' ) => 'typicons',
					esc_html__( 'Entypo', 'js_composer' ) => 'entypo',
					esc_html__( 'Linecons', 'js_composer' ) => 'linecons',
					esc_html__( 'Tesla Icons', 'js_composer' ) => 'icomoon'
				),
				'admin_label' => false,
				'param_name' => 'type',
				'description' => esc_html__( 'Select icon library.', 'js_composer' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'js_composer' ),
				'value' => array(
					esc_html__( 'Font Awesome', 'js_composer' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'js_composer' ) => 'openiconic',
					esc_html__( 'Typicons', 'js_composer' ) => 'typicons',
					esc_html__( 'Entypo', 'js_composer' ) => 'entypo',
					esc_html__( 'Linecons', 'js_composer' ) => 'linecons',
					esc_html__( 'Tesla Icons', 'js_composer' ) => 'icomoon'
				),
				'admin_label' => false,
				'param_name' => 'type',
				'description' => esc_html__( 'Select icon library.', 'js_composer' ),
				'dependency' => array(
					'element' => 'add_icon',
					'value' => 'true',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'js_composer' ),
				'param_name' => 'icon_fontawesome',
				'value' => 'fa fa-adjust', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false,
					// default true, display an "EMPTY" icon?
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'js_composer' ),
				'param_name' => 'icon_openiconic',
				'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'openiconic',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'openiconic',
				),
				'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'js_composer' ),
				'param_name' => 'icon_typicons',
				'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'typicons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'typicons',
				),
				'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'js_composer' ),
				'param_name' => 'icon_entypo',
				'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'entypo',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'entypo',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'js_composer' ),
				'param_name' => 'icon_linecons',
				'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'linecons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'linecons',
				),
				'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'js_composer' ),
				'param_name' => 'icon_icomoon',
				'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'icomoon',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'icomoon',
				),
				'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
			));
	return $icons;
}
$tt_icons = tt_icons();

global $vc_add_css_animation;

$vc_add_css_animation = array(
	'type' => 'dropdown',
	'heading' => esc_html__( 'CSS Animation', 'js_composer' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		esc_html__( 'No', 'js_composer' ) => '',
		esc_html__( 'Top to bottom', 'js_composer' ) => 'top-to-bottom',
		esc_html__( 'Bottom to top', 'js_composer' ) => 'bottom-to-top',
		esc_html__( 'Left to right', 'js_composer' ) => 'left-to-right',
		esc_html__( 'Right to left', 'js_composer' ) => 'right-to-left',
		esc_html__( 'Appear from center', 'js_composer' ) => 'appear'
	),
	'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'js_composer' )
);

global $vc_column_width_list;
$vc_column_width_list = array(
	esc_html__('1 column - 1/12', 'js_composer') => '1/12',
	esc_html__('2 columns - 1/6', 'js_composer') => '1/6',
	esc_html__('3 columns - 1/4', 'js_composer') => '1/4',
	esc_html__('4 columns - 1/3', 'js_composer') => '1/3',
	esc_html__('5 columns - 5/12', 'js_composer') => '5/12',
	esc_html__('6 columns - 1/2', 'js_composer') => '1/2',
	esc_html__('7 columns - 7/12', 'js_composer') => '7/12',
	esc_html__('8 columns - 2/3', 'js_composer') => '2/3',
	esc_html__('9 columns - 3/4', 'js_composer') => '3/4',
	esc_html__('10 columns - 5/6', 'js_composer') => '5/6',
	esc_html__('11 columns - 11/12', 'js_composer') => '11/12',
	esc_html__('12 columns - 1/1', 'js_composer') => '1/1'
);

/* Custom Heading element
----------------------------------------------------------- */
vc_map( array(
	'name' => esc_html__( 'Custom Heading', 'js_composer' ),
	'base' => 'vc_custom_heading',
	'icon' => 'icon-wpb-ui-custom_heading',
	'show_settings_on_create' => true,
	'category' => esc_html__( 'Content', 'js_composer' ),
	'description' => esc_html__( 'Text with Google fonts', 'js_composer' ),
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Text source', 'js_composer' ),
			'param_name' => 'source',
			'value' => array(
				esc_html__( 'Custom text', 'js_composer' ) => '',
				esc_html__( 'Post or Page Title', 'js_composer' ) => 'post_title'
			),
			'std' => '',
			'description' => esc_html__( 'Select text source.', 'js_composer' )
		),
		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Text', 'js_composer' ),
			'param_name' => 'text',
			'admin_label' => true,
			'value' => esc_html__( 'This is custom heading element', 'js_composer' ),
			'description' => esc_html__( 'If you are using non-latin characters be sure to activate them under Settings/Visual Composer/General Settings.', 'js_composer' ),					'description' => esc_html__( 'Note: If you are using non-latin characters be sure to activate them under Settings/Visual Composer/General Settings.', 'js_composer' ),
		),

		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Make uppercase', 'js_composer' ),
			'param_name' => 'make_uppercase',
			'value' => array( esc_html__( 'Yes, please', 'js_composer' ) => 'yes' ),
			'description' => esc_html__( 'Transform text to uppercase', 'js_composer' ),
		),

		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'No margin bottom', 'js_composer' ),
			'param_name' => 'no_bottom',
			'value' => array( esc_html__( 'Yes, please', 'js_composer' ) => 'yes' ),
			'description' => esc_html__( 'Best for section titles, remove bottom space.', 'js_composer' ),
		),
		array(
			'type' => 'vc_link',
			'heading' => esc_html__( 'URL (Link)', 'js_composer' ),
			'param_name' => 'link',
			'description' => esc_html__( 'Add link to custom heading.', 'js_composer' ),
			// compatible with btn2 and converted from href{btn1}
		),
		array(
			'type' => 'font_container',
			'param_name' => 'font_container',
			'value' => '',
			'settings' => array(
				'fields' => array(
					'tag' => 'h2', // default value h2
					'text_align',
					'font_size',
					'line_height',
					'color',
					//'font_style_italic'
					//'font_style_bold'
					//'font_family'

					'tag_description' => esc_html__( 'Select element tag.', 'js_composer' ),
					'text_align_description' => esc_html__( 'Select text alignment.', 'js_composer' ),
					'font_size_description' => esc_html__( 'Enter font size.', 'js_composer' ),
					'line_height_description' => esc_html__( 'Enter line height.', 'js_composer' ),
					'color_description' => esc_html__( 'Select color for your element.', 'js_composer' ),
					//'font_style_description' => esc_html__('Put your description here','js_composer'),
					//'font_family_description' => esc_html__('Put your description here','js_composer'),
				),
			),
			// 'description' => esc_html__( '', 'js_composer' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Letter Spacing', 'js_composer' ),
			'param_name' => 'letter_spacing',
			'description' => esc_html__( 'Enter letter spacing.', 'js_composer' ),
		),

		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Use theme default font family?', 'js_composer' ),
			'param_name' => 'use_theme_fonts',
			'value' => array( esc_html__( 'Yes', 'js_composer' ) => 'yes' ),
			'description' => esc_html__( 'Use font family from the theme.', 'js_composer' ),
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Select font weight', 'js_composer' ),
			'param_name' => 'fnt_weight',
			'value' => array(
				'300',
				'400',
				'500',
				'600',
				'700'
			),
			'std' => '300',
			'dependency' => array(
				'element' => 'use_theme_fonts',
				'value' => 'yes',
			),
		),


		array(
			'type' => 'google_fonts',
			'param_name' => 'google_fonts',
			'value' => 'font_family:Montserrat|font_style:400%20regular%3A400%3Anormal', // default
			//'font_family:'.rawurlencode('Abril Fatface:400').'|font_style:'.rawurlencode('400 regular:400:normal')
			// this will override 'settings'. 'font_family:'.rawurlencode('Exo:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic').'|font_style:'.rawurlencode('900 bold italic:900:italic'),
			'settings' => array(
				//'no_font_style' // Method 1: To disable font style
				//'no_font_style'=>true, // Method 2: To disable font style
				'fields' => array(
					//'font_family' => 'Abril Fatface:regular',
					//'Exo:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic',// Default font family and all available styles to fetch
					//'font_style' => '400 regular:400:normal',
					// Default font style. Name:weight:style, example: "800 bold regular:800:normal"
					'font_family_description' => esc_html__( 'Select font family.', 'js_composer' ),
					'font_style_description' => esc_html__( 'Select font styling.', 'js_composer' )
				)
			),
			'dependency' => array(
				'element' => 'use_theme_fonts',
				'value_not_equal_to' => 'yes',
			),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'js_composer' ),
			'param_name' => 'css',
			// 'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
			'group' => esc_html__( 'Design Options', 'js_composer' )
		)
	),
) );


/* TT Blog Feed
----------------------------------------------------------- */
$post_categories = get_terms( 'category', array( 'hide_empty' => 0 ) );
$post_cats['All Categories'] = '0';

if(!is_wp_error($post_categories)) {
	foreach($post_categories as $category) 
		$post_cats[$category->name] = $category->term_id;
}

vc_map( array(
	'name' => esc_html__( 'Blog Feed', 'js_composer' ),
	'base' => 'tt_blog_list',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ),
	'description' => esc_html__( 'Tesla Blog Feed', 'js_composer' ),
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Category', 'js_composer' ),
			'value' => $post_cats,
			'param_name' => 'tesla_category',
			'description' => esc_html__( 'Select from which category you want the posts to be displayed.', 'js_composer' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Number of posts', 'js_composer' ),
			'param_name' => 'nr_posts',
			'description' => esc_html__( 'Set a number of posts to be displayed. Default "2".', 'js_composer' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Offset Nr', 'js_composer' ),
			'param_name' => 'nr_posts_offset',
			'description' => esc_html__( 'Set a number of posts to be offsetted. Default "0".', 'js_composer' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently, add custom class from CSS.', 'js_composer' ),
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'js_composer' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design options', 'js_composer' )
		)
	),
) );

/* TT Instagram
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Instagram', 'js_composer' ),
	'base' 		=> 'tt_instagram',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ),
	'description' => esc_html__( 'Tesla Instagram', 'js_composer' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Nr. of images', 'js_composer' ),
			'param_name' => 'images_nr',
			'value' => '3',
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Username', 'js_composer' ),
			'param_name' => 'is_username',
			'value' => '',
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Cache (in hours)', 'js_composer' ),
			'param_name' => 'is_cache',
			'value' => '',
		),
		
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'js_composer' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'js_composer' )
		)
	)
) );

/* TT Simple Link
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Link', 'js_composer' ),
	'base' 		=> 'tt_link',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ),
	'description' => esc_html__( 'Tesla Simple Link', 'js_composer' ),
	'params' => array(
		array(
			'type' => 'vc_link',
			'heading' => esc_html__( '(Link)', 'js_composer' ),
			'param_name' => 'link',
			'description' => esc_html__( 'Add url to element.', 'js_composer' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'js_composer' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'js_composer' )
		)
	)
) );

/* TT Counter
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Stats Counter', 'js_composer' ),
	'base' 		=> 'tt_counter',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ),
	'description' => esc_html__( 'Tesla Stats Counter', 'js_composer' ),
	'params' => array(
		$tt_icons[0],
		$tt_icons[2],
		$tt_icons[3],
		$tt_icons[4],
		$tt_icons[5],
		$tt_icons[6],
		$tt_icons[7],

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Icon Color', 'js_composer' ),
			'param_name' => 'color_icon',
			'description' => esc_html__( 'Select icon color.', 'js_composer' ),
		),
		
		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Number', 'js_composer' ),
			'param_name' => 'number',
			'admin_label' => true,
			'value' => esc_html__( 'Insert here stats number', 'js_composer' ),
		),

		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Title', 'js_composer' ),
			'param_name' => 'title',
			'admin_label' => true,
			'value' => esc_html__( 'This is a service title', 'js_composer' ),
		),

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Text Color', 'js_composer' ),
			'param_name' => 'text_color',
			'description' => esc_html__( 'Select text color for title & description.', 'js_composer' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'js_composer' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'js_composer' )
		)
	)
) );

/* TT Progress Bar
----------------------------------------------------------- */
vc_map( array(
	'name' => __( 'Progress Bar', 'js_composer' ),
	'base' => 'tt_progressbar',
	'icon' => 'icon-wpb',
	'category' => __( 'TeslaThemes', 'js_composer' ),
	'description' => __( 'Tesla Progress Block', 'js_composer' ),
	"params" => array(
		array(
			'type' => 'textfield',
			'heading' => __( 'Title', 'js_composer' ),
			'param_name' => 'title',
			'description' => __( 'Insert here block title.', 'js_composer' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => __( 'Width', 'js_composer' ),
			'param_name' => 'b_width',
			'description' => __( 'Insert here progress bar width in %', 'js_composer' ),
			'admin_label' => true,
		),

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Bar Background', 'js_composer' ),
			'param_name' => 'bar_bg',
			'description' => esc_html__( 'Select background color for progress bar.', 'js_composer' ),
		),

		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
		),

		array(
			'type' => 'css_editor',
			'heading' => __( 'Css', 'js_composer' ),
			'param_name' => 'css',
			// 'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
			'group' => __( 'Design options', 'js_composer' )
		)
	)
) );

/* TT Subscribe Form
----------------------------------------------------------- */
vc_map( array(
	'name' => __( 'Subscribe Form', 'js_composer' ),
	'base' => 'tt_subscribeform',
	'icon' => 'icon-wpb',
	'category' => __( 'TeslaThemes', 'js_composer' ),
	'description' => __( 'Tesla Subscribe Form', 'js_composer' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => __( 'Style particular content element differently, add custom class from CSS.', 'js_composer' ),
		),

		array(
			'type' => 'css_editor',
			'heading' => __( 'Css', 'js_composer' ),
			'param_name' => 'css',
			'group' => __( 'Design options', 'js_composer' )
		)
	),
) );

/* TT Google Map
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Google Map', 'js_composer' ),
	'base' 		=> 'tt_gmap',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ),
	'description' => esc_html__( 'Create Google Map', 'js_composer' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Map height', 'js_composer' ),
			'param_name' => 'map_height',
			'value' => '',
			'description' => esc_html__( 'Set map height. Please provide value with unit of measurement (px, cm, in)'
				, 'js_composer' ),
		),
		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Map Pin', 'js_composer' ),
			'param_name' => 'map_pin',
			'value' => '', // default video url
			'description' => esc_html__( 'Set pin for map', 'js_composer' )
		),

		array(
			'type' => 'textarea_html',
			'heading' => esc_html__( 'Description', 'js_composer' ),
			'param_name' => 'content',
			'value' => '',
			'description' => esc_html__( 'Insert here infobox description'
				, 'js_composer' ),
		),

		array(
			'type' => 'checkbox',
			'heading' => __( 'Hide Social Links?', 'js_composer' ),
			'param_name' => 'hide_social',
			'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' )
		),
		array(
			'type' => 'google_map',
			'heading' => esc_html__( 'Google Map Editor', 'js_composer' ),
			'param_name' => 'map_settings',
			'group' => esc_html__( 'Map Editor', 'js_composer' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'js_composer' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'js_composer' )
		)
	)
) );

/* TT Custom Heading
----------------------------------------------------------- */
vc_map( array(
	'name' => esc_html__( 'Section Title', 'js_composer' ),
	'base' => 'tt_section_header',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ),
	'description' => esc_html__( 'Tesla Section Title', 'js_composer' ),
	'params' => array(
		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Section Title', 'js_composer' ),
			'param_name' => 'title',
			'admin_label' => true,
			'value' => esc_html__( 'This is custom heading title', 'js_composer' ),
		),

		array(
			'type' => 'font_container',
			'param_name' => 'font_container_title',
			'value' => '',
			'settings' => array(
				'fields' => array(
					'text_align' => 'center',
					'font_size',
					'line_height',
					'color',
					'text_align_description' => esc_html__( 'Select text alignment.', 'js_composer' ),
					'font_size_description' => esc_html__( 'Enter font size.', 'js_composer' ),
					'line_height_description' => esc_html__( 'Enter line height.', 'js_composer' ),
					'color_description' => esc_html__( 'Select color for your element.', 'js_composer' ),
				),
			),
		),

		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Section Description', 'js_composer' ),
			'param_name' => 'subtext',
			'admin_label' => true,
			'value' => esc_html__( 'This is a custom heading description', 'js_composer' ),
		),

		array(
			'type' => 'font_container',
			'param_name' => 'font_container_desc',
			'value' => '',
			'settings' => array(
				'fields' => array(
					'text_align' => 'center',
					'font_size',
					'line_height',
					'color',
					'text_align_description' => esc_html__( 'Select text alignment.', 'js_composer' ),
					'font_size_description' => esc_html__( 'Enter font size.', 'js_composer' ),
					'line_height_description' => esc_html__( 'Enter line height.', 'js_composer' ),
					'color_description' => esc_html__( 'Select color for your element.', 'js_composer' ),
				),
			),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a existing class name (black, white, hero-section) or another custom class from CSS.', 'js_composer' ),
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'js_composer' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design options', 'js_composer' )
		)
	),
) );

/* TT Service */
vc_map( array(
	'name' => esc_html__( 'Service Box', 'js_composer' ),
	'base' => 'tt_service',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ), 'description' => esc_html__( 'Tesla Service Box', 'js_composer' ),
	"params" => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Type', 'js_composer' ),
			'value' => array(
				esc_html__( 'Type 1', 'js_composer' ) => 'type-1',
				esc_html__( 'Type 2', 'js_composer' ) => 'type-2',
				esc_html__( 'Type 3', 'js_composer' ) => 'type-3',
				esc_html__( 'Type 3 (left)', 'js_composer' ) => 'type-4',
			),
			'param_name' => 'service_type',
			'description' => esc_html__( 'Select service style type.', 'js_composer' ),
			'admin_label' => true,
		),

		$tt_icons[0],
		$tt_icons[2],
		$tt_icons[3],
		$tt_icons[4],
		$tt_icons[5],
		$tt_icons[6],
		$tt_icons[7],

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Icon Color', 'js_composer' ),
			'param_name' => 'color_icon',
			'description' => esc_html__( 'Select icon color.', 'js_composer' ),
		),

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Icon Background', 'js_composer' ),
			'param_name' => 'background_icon',
			'description' => esc_html__( 'Select icon background (not working for all service types).', 'js_composer' ),
		),
		
		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Title', 'js_composer' ),
			'param_name' => 'title',
			'admin_label' => true,
			'value' => esc_html__( 'This is a service title', 'js_composer' ),
		),

		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Description', 'js_composer' ),
			'param_name' => 'subtext',
			'admin_label' => false,
			'value' => esc_html__( 'This is a small service description', 'js_composer' ),
			'description' => esc_html__( 'If you are using non-latin characters be sure to activate them under Settings/Visual Composer/General Settings.', 'js_composer' ),
			'description' => esc_html__( 'Note: If you are using non-latin characters be sure to activate them under Settings/Visual Composer/General Settings.', 'js_composer' ),
		),

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Text Color', 'js_composer' ),
			'param_name' => 'text_color',
			'description' => esc_html__( 'Select text color for title & description.', 'js_composer' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'js_composer' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design options', 'js_composer' )
		)
	)
) );

/* TT Testimonials
----------------------------------------------------------- */
$testimonials_categories = get_terms( 'testimonials_tax', array( 'hide_empty' => 0 ) );
$testimonials_cats['All'] = '';

if(!is_wp_error($testimonials_categories)) {
	foreach($testimonials_categories as $category) 
		$testimonials_cats[$category->name] = $category->slug;
}

vc_map( array(
	'name' => esc_html__( 'Testimonials', 'js_composer' ),
	'base' => 'tt_testimonials',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ),
	'description' => esc_html__( 'Tesla Testimonials Items', 'js_composer' ),
	"params" => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Category', 'js_composer' ),
			'value' => $testimonials_cats,
			'param_name' => 'category',
			'description' => esc_html__( 'Select default category.', 'js_composer' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Nr. of items', 'js_composer' ),
			'param_name' => 'nr',
			'description' => esc_html__( 'Insert here number of items to show.', 'js_composer' ),
		),

		array(
			'type' => 'dropdown',
			'heading' => __( 'Columns', 'js_composer' ),
			'value' => array(
				__( '2 Columns', 'js_composer' ) => '2|2',
				__( '3 Columns', 'js_composer' ) => '3|3',
				__( '4 Columns', 'js_composer' ) => '4|3',
			),
			'param_name' => 'columns',
			'description' => __( 'Select column size.', 'js_composer' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'js_composer' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'js_composer' )
		)
	)
) );

/* TT Portfolio
----------------------------------------------------------- */
$port_categories = get_terms( 'tt_portfolio_tax', array( 'hide_empty' => 0 ) );
$port_cats['All Categories'] = '*';

if(!is_wp_error($port_categories)) {
	foreach($port_categories as $category) 
		$port_cats[$category->name] = $category->slug;
}

vc_map( array(
	'name' => __( 'Portfolio Feed', 'js_composer' ),
	'base' => 'tt_portfolio',
	'icon' => 'icon-wpb',
	'category' => __( 'TeslaThemes', 'js_composer' ),
	'description' => __( 'Tesla Portfolio Items', 'js_composer' ),
	"params" => array(

		array(
			'type' => 'dropdown',
			'heading' => __( 'Category', 'js_composer' ),
			'value' => $port_cats,
			'param_name' => 'item_category',
			'description' => __( 'Select default category.', 'js_composer' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => __( 'Nr. of items', 'js_composer' ),
			'param_name' => 'nr',
			'description' => __( 'Insert here number of items to show.', 'js_composer' ),
			'admin_label' => true,
		),

		array(
            'type'  =>  'textfield',
            'heading'   =>  __('Offset nr','js_composer'),
            'param_name'    =>  'offset',
            'description'   =>  __('Set a number of portfolio items to be offsetted. Default "0"','js_composer'),
            'admin_label' => true,
        ),

		array(
			'type' => 'dropdown',
			'heading' => __( 'Columns', 'js_composer' ),
			'value' => array(
				__( '2 Columns', 'js_composer' ) => 'col-xs-6',
				__( '3 Columns', 'js_composer' ) => 'col-xs-6 col-md-4',
				__( '4 Columns', 'js_composer' ) => 'col-xs-6 col-md-4 col-lg-3',
			),
			'param_name' => 'columns',
			'description' => __( 'Select column size.', 'js_composer' ),
		),
		
		array(
			'type' => 'checkbox',
			'heading' => __( 'Hide Filters?', 'js_composer' ),
			'param_name' => 'hide_filters',
			'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
			'description' => __( 'Hide portfolio filters.', 'js_composer' ),
		),

		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
		),

		array(
			'type' => 'css_editor',
			'heading' => __( 'CSS box', 'js_composer' ),
			'param_name' => 'css',
			'group' => __( 'Design Options', 'js_composer' )
		)
	)
) );

/* TT Pricing */
vc_map( array(
	'name' => esc_html__( 'Pricing Table', 'js_composer' ),
	'base' => 'tt_pricing',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ),
	'description' => esc_html__( 'Tesla Pricing Table', 'js_composer' ),
	"params" => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Price', 'js_composer' ),
			'param_name' => 'price',
			'description' => esc_html__( '', 'js_composer' ),
			'admin_label' => true, 
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Plan', 'js_composer' ),
			'param_name' => 'plan',
			'description' => esc_html__( '', 'js_composer' ),
			'admin_label' => true, 
		),

		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Description', 'js_composer' ),
			'param_name' => 'description',
			'description' => esc_html__( '', 'js_composer' ),
		),

		array(
			'type' => 'exploded_textarea',
			'heading' => esc_html__( 'Features', 'js_composer' ),
			'param_name' => 'features',
			'description' => esc_html__( '', 'js_composer' ),
		),

		array(
			'type' => 'vc_link',
			'heading' => esc_html__( 'Button (Link)', 'js_composer' ),
			'param_name' => 'link',
			'description' => esc_html__( 'Add link to button.', 'js_composer' ),
		),

		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Popular?', 'js_composer' ),
			'param_name' => 'vip_pricing',
			'value' => array( esc_html__( 'Yes, please', 'js_composer' ) => 'yes' ),
			'description' => ''
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'js_composer' ),
			'param_name' => 'css',
			// 'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
			'group' => esc_html__( 'Design options', 'js_composer' )
		)
	)
) );

/* TT Breadcrumbs */
vc_map( array(
	'name' => esc_html__( 'Breadcrumbs', 'js_composer' ),
	'base' => 'tt_breadcrumbs',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ),
	'description' => esc_html__( 'Tesla Breadcrumbs', 'js_composer' ),
	"params" => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'js_composer' ),
			'param_name' => 'css',
			// 'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
			'group' => esc_html__( 'Design options', 'js_composer' )
		)
	)
) );

/* TT Video Toggle
----------------------------------------------------------- */
vc_map( array(
	'name' => esc_html__( 'Video Toggle', 'js_composer' ),
	'base' => 'tt_video_toggle',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'js_composer' ),
	'description' => esc_html__( 'Tesla video toggle for popup', 'js_composer' ),
	'params' => array(
		array(
			'type' => 'textarea_raw_html',
			'heading' => esc_html__( 'Embed Code', 'js_composer' ),
			'param_name' => 'video_url',
			'description' => esc_html__( 'Insert here embed code', 'js_composer' ),
		),

		array(
			'type' => 'dropdown',
			'heading' => __( 'Alignment', 'js_composer' ),
			'value' => array(
				__( 'Inline', 'js_composer' ) => 'inline',
				__( 'Left', 'js_composer' ) => 'left',
				__( 'Right', 'js_composer' ) => 'right',
				__( 'Center', 'js_composer' ) => 'center',
			),
			'param_name' => 'alignment',
			'description' => __( 'Select alignment.', 'js_composer' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'js_composer' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design options', 'js_composer' )
		)
	),
) );

/* TT Team Member
----------------------------------------------------------- */
$member_categories = get_terms( 'tt_members_tax', array( 'hide_empty' => 0 ) );
$member_cats['All Categories'] = '*';

if(!is_wp_error($member_categories)) {
	foreach($member_categories as $category) 
		$member_cats[$category->name] = $category->slug;
}

vc_map( array(
	'name' => __( 'Team Members', 'js_composer' ),
	'base' => 'tt_team',
	'icon' => 'icon-wpb',
	'category' => __( 'TeslaThemes', 'js_composer' ),
	'description' => __( 'Tesla Team Members', 'js_composer' ),
	"params" => array(

		array(
			'type' => 'dropdown',
			'heading' => __( 'Category', 'js_composer' ),
			'value' => $member_cats,
			'param_name' => 'category',
			'description' => __( 'Select default category.', 'js_composer' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => __( 'Nr. of items', 'js_composer' ),
			'param_name' => 'nr',
			'description' => __( 'Insert here number of items to show.', 'js_composer' ),
			'admin_label' => true,
		),

		array(
            'type'  =>  'textfield',
            'heading'   =>  __('Offset nr','js_composer'),
            'param_name'    =>  'offset',
            'description'   =>  __('Set a number of items to be offsetted. Default "0"','js_composer'),
            'admin_label' => true,
        ),

		array(
			'type' => 'dropdown',
			'heading' => __( 'Columns', 'js_composer' ),
			'value' => array(
				__( '2 Columns', 'js_composer' ) => 'col-sm-6|2',
				__( '3 Columns', 'js_composer' ) => 'col-sm-4|3',
				__( '4 Columns', 'js_composer' ) => 'col-md-3 col-sm-4|4',
			),
			'param_name' => 'columns',
			'description' => __( 'Select column size.', 'js_composer' ),
		),

		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Use Slider?', 'js_composer' ),
			'param_name' => 'use_slider',
			'value' => array( esc_html__( 'Yes, please', 'js_composer' ) => 'yes' ),
		),
		
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'js_composer' ),
			'param_name' => 'el_class',
			'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
		),

		array(
			'type' => 'css_editor',
			'heading' => __( 'CSS box', 'js_composer' ),
			'param_name' => 'css',
			'group' => __( 'Design Options', 'js_composer' )
		)
	)
) );

vc_add_param( 'vc_btn', array(
	'type' => 'checkbox',
	'heading' => esc_html__( 'Use theme button?', 'js_composer' ),
	'param_name' => 'theme_btn',
	'value' => array( esc_html__( 'Yes, please', 'js_composer' ) => 'yes' ),
	'description' => esc_html__( 'Use template default button style', 'js_composer' ),
));