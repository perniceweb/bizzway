<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$alignment = !empty($alignment) ? 'align'.$alignment : 'aligninline';
$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';?>

<div class="video-toggle <?php echo esc_attr($alignment.' '.$css_class.' '.$el_class);?>" data-embed="<?php echo esc_attr($video_url);?>"></div>

<!-- Video Popup -->
<div class="popup-wrapper video-popup">
	<!-- Close Video Popup -->
	<span class="close-video-popup-toggle"></span>
	
	<div class="popup-inner-content">
		<div class="media-wrapper">
			<div class="responsive-media ratio16by9"></div>
		</div>
	</div>
</div>
