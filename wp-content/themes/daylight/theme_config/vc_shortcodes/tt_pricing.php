<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$url = vc_build_link( $link );?>

<div class="pricing-table <?php echo esc_attr($css_class.' '.$el_class); if($vip_pricing) echo esc_attr(' vip');?>">
	<?php if($vip_pricing) print '<i class="i-icon icon-trophy"></i>'; ?>
	<h4 class="title"><?php print $plan;?></h4>
	<span class="pricing"><?php print $price;?></span>
	<span class="description"><?php print $description;?></span>
	<ul class="features">
		<?php if(!empty($features)) {
			$features = explode(',', $features);
			foreach ($features as $key => $item)
				print '<li>'.$item.'</li>';
		} ?>
	</ul>

	<?php if(!empty($url['url'])):?>
		<a class="btn white buy-btn" href="<?php echo esc_attr($url['url']) ?>" title="<?php echo esc_attr($url['title']) ?>" target="<?php echo ( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self') ?>"><?php echo esc_attr($url['title']) ?></a>
	<?php endif;?>
</div>




