<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$post_number = $nr_posts ? $nr_posts : 4;

$args = array(
    'post_status'   =>  'publish',
    'post_type'     =>  'post',
    'showposts' => $post_number,
);

if(!empty($nr_posts_offset))
    $args['offset'] = $nr_posts_offset;

if(!empty($tesla_category))
    $args['cat'] = $tesla_category;

$tt_query = new WP_Query($args); ?>

<div class="blog-feed-wrapper <?php echo esc_attr($css_class.' '.$el_class);?>">
	<div class="row isotope-container">
	<?php while($tt_query->have_posts()) : $tt_query->the_post(); $post_id = get_the_ID(); ?>
		<div class="col-md-6 isotope-item">
			<div <?php post_class('blog-post-item'); ?>>
				<?php if(has_post_thumbnail()):?>
				<div class="cover">
					<a href="<?php the_permalink();?>">
						<?php the_post_thumbnail(array(200,195));?>
						<span class="post-date"><?php the_time(get_option('date_format'));?></span>
					</a>
				</div>
				<?php endif;?>

				<div class="content">
					<h3 class="title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
					<p class="description"><?php print tt_excerpt($post_id,160);?></p>

					<div class="post-meta">
						<span class="comments"><?php comments_number( esc_html__('0','daylight'), esc_html__('1','daylight'), '%'); ?></span>
        				<span class="likes <?php echo isset($_COOKIE['post_likes_'. $post_id]) ? esc_attr('liked') : ''; ?>" data-id="<?php echo esc_attr($post_id);?>"><?php print get_post_meta($post_id, 'post_likes', true) ? get_post_meta($post_id, 'post_likes', true) : '0' ;?></span>
						<a class="read-more" href="<?php the_permalink();?>"><?php esc_html_e('read more','daylight');?></a>
					</div>
				</div>
			</div>
		</div>		
	<?php endwhile; wp_reset_postdata();?>
	</div>
</div>