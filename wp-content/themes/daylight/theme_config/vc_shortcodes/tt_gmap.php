<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$bg_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$theme_class = $bg_class . ' ' . $el_class;

wp_enqueue_script( 'g-map' );

$wrapper_attributes[] = !empty( $map_settings ) 
	? sprintf( 'data-map-settings="%s"', $map_settings ) : '';

$pin_src = !empty($map_pin) 
	? wp_get_attachment_image_src( $map_pin, 'full' ) : '';
if(!empty($pin_src[0])) {
	$wrapper_attributes[] = sprintf( 'data-pin="%s"', $pin_src[0] );
}

$map_height = !empty( $map_height ) 
	? sprintf( 'style=" min-height: %s;"', $map_height ) : ''; ?>

<div class="map-wrapper">
	<div class="map-canvas <?php echo esc_attr($theme_class);?>" <?php print implode( ' ', $wrapper_attributes );?> <?php print $map_height;?>></div>
	<?php if($content):?>
		<div class="infobox">
			<?php print $content;?>

			<?php if(!$hide_social):?>
			<ul class="socials">
				<?php $social_platforms = array('facebook','twitter','google','pinterest','instagram','linkedin','dribbble','behance','youtube','flickr','rss');
				foreach($social_platforms as $platform): 
				    if (_go('social_platforms_' . $platform)): ?>
				        <li>
				        	<a class="<?php print $platform === 'google' ? 'google-plus' : $platform; ?>" href="<?php echo esc_url(_go('social_platforms_' . $platform)); ?>"><i class="fa fa-<?php print $platform === 'google' ? 'google-plus' : $platform; ?>"></i></a>
				        </li>
				    <?php endif;
				endforeach; ?>
			</ul>
			<?php endif;?>
		</div>
	<?php endif;?>
</div>






