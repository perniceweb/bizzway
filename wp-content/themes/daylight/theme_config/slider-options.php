<?php

return array(
	'testimonials' => array(
		'name' => 'Testimonials',
		'term' => 'testimonial',
		'term_plural' => 'testimonials',
		'order' => 'ASC',
		'has_single' => false,
		'post_options' => array('supports'=>array('title','editor','thumbnail')),
		'taxonomy_options' => array('show_ui'=>true),
		'options' => array(),
		'icon' => 'icons/favicon.png',
		'output_default' => 'main',
		'options' => array(
			'position' => array(
				'type' => 'line',
				'description' => 'Position of the testimonial author',
				'title' => 'Position'
			),
		),
		'output' => array(
			'main' => array(
				'shortcode' => 'tt_testimonials',
				'view' => 'views/testimonials-view',
				'shortcode_defaults' => array(	
					'category' => '',
					'offset' => '',
					'columns' => '',
					'el_class' => '',
					'css' => '',
					'nr' => 5000
				)
			),			
		)
	),

	'tt_portfolio' => array(
		'name' => 'Portfolio',
		'term' => 'Portfolio',
		'term_plural' => 'Items in Portfolio',
		'order' => 'DESC',
		'has_single' => true,
		'url' => _go('portfolio_url') ? _go('portfolio_url') : 'tt-portfolio',
		'post_options' => array(
			'supports'=> array( 'title','editor','comments'),
			'taxonomies' => array('post_tag'),
			'has_archive'=>true
			),
		'taxonomy_options' => array('show_ui' => true),
		'options' => array(
			'cover_image' => array(
				'type' => 'image',
				'description' => 'Portfolio item cover/thumbnail (shown in the portfolio grids).',
				'title' => 'Portfolio Cover',
				'default' => 'holder.js/360x243/auto'
			),
			'full_image' => array(
				'type' => 'image',
				'description' => 'Portfolio item full size image ( as big as you need  )',
				'title' => 'Full Size',
				'default' => 'holder.js/1140x594/auto'
			),

			'author'	=>	array(
				'title' => 'Author',
				'description' => 'Insert project author.',
				'type' => 'line',
				'default' => ''
			),

			'client'	=>	array(
				'title' => 'Client',
				'description' => 'Insert project client.',
				'type' => 'line',
				'default' => ''
			),
		),
		'icon' => 'icons/favicon.png',
		'output' => array(
			'main' => array(
				'shortcode' => 'tt_portfolio',
				'view' => 'views/portfolio-view',
				'shortcode_defaults' => array(
					'item_category' => '',
					'columns' => '',
					'nr' =>	'5000',
					'offset' => '',
					'hide_filters' => '',
					'el_class' => '',
					'css' => ''
				)
			),
		),
	),

	'tt_members' => array(
		'name' => 'Team',
		'term' => 'Team member',
		'term_plural' => 'Team members',
		'order' => 'ASC',
		'options' => array(
			'image' => array(
				'type' => 'image',
				'description' => 'Image of the team member',
				'title' => 'Image',
				'default' => 'holder.js/263x270/auto'
			),
			
			'position' => array(
				'type' => 'line',
				'description' => 'Position of the team member',
				'title' => 'Position'
			),

			'social_links' => array(
                'type' => array(
                    'title' => array(
                        'type' => 'line',
                        'title' => 'Social Title',
                    ),
                    'url' => array(
                        'type' => 'line',
                        'title' => 'Social URL'
                    )
                ),
                'description' => 'Insert here social networks one by one.',
                'title' => 'Social Networks',
                'multiple' => true,
                'group' => true
            ),
		),
		'output' => array(
			'main' => array(
				'shortcode' => 'tt_team',
				'view' => 'views/team-view',
				'shortcode_defaults' => array(
					'category' => '',
					'columns' => '',
					'use_slider' => '',
					'offset' => '',
					'el_class' => '',
					'css' => '',
					'nr'=>'5000',
				)
			),
		),
		'icon' => 'icons/favicon.png'
	),
);