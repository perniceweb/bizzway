<?php 

$menu_style = get_post_meta(tt_get_page_id(), THEME_NAME.'_header_type', true);

return array(
	'metaboxes'	=>	array(
		array(
			'id'             => 'page_metabox',            // meta box id, unique per meta box
			'title'          => esc_html_x('Page Settings','meta boxes','daylight'),   // meta box title
			'post_type'      => array('page'),		// post types, accept custom post types as well, default is array('post'); optional
			'taxonomy'       => array(),    // taxonomy name, accept categories, post_tag and custom taxonomies
			'context'		 => 'normal',						// where the meta box appear: normal (default), advanced, side; optional
			'priority'		 => 'low',						// order of meta box: high (default), low; optional
			'input_fields'   => array(
                'sidebar_position'  =>  array(
					'name'  =>  esc_html_x('Sidebar Position','meta boxes','daylight'),
					'type'  =>  'select',
					'values'    =>  array(
							'full_width'    =>  esc_html_x('No Sidebar/Full Width','meta boxes','daylight'),
							'right'         =>  esc_html_x('Right','meta boxes','daylight'),
							'left'          =>  esc_html_x('Left','meta boxes','daylight'),
						),
					'std'   =>  'right'  //default value selected
				),

                'header_type'=>array(
					'name'=> esc_html_x('Header Type','meta boxes','daylight'),
					'type'=>'select',
					'values'=>array(
							'sticky'=>esc_html_x('Sticky','meta boxes','daylight'),
							'no-sticky'=>esc_html_x('No Sticky','meta boxes','daylight'),
					),
				'std'=> $menu_style  ? $menu_style : (_go('header_style') ? _go('header_style') : 'no-sticky')
				),

				'page_background'=>array(
		    		'name'=>'Hero Background',
		    		'type'=>'image',
		    		'desc'=>'If you want a different background to be used for this page, then you can upload it here.'
		    	),

		    	'hide_hero'=>array(
		    		'name' => 'Hide hero-section?',
		    		'type' => 'checkbox',
		    		'desc'=> "Check if you want to hide hero-section, usually it's used when you create page with Visual Composer <br/> Note: Will not work on blog page" ,
	    		),
			)
		),

		array(
			'id'             => 'post_metabox',            // meta box id, unique per meta box
			'title'          => esc_html_x('Post Settings','meta boxes','daylight'),   // meta box title
			'post_type'      => array('post'),		// post types, accept custom post types as well, default is array('post'); optional
			'taxonomy'       => array(),    // taxonomy name, accept categories, post_tag and custom taxonomies
			'context'		 => 'normal',						// where the meta box appear: normal (default), advanced, side; optional
			'priority'		 => 'low',						// order of meta box: high (default), low; optional
			'input_fields'   => array(
				'sidebar_position'  =>  array(
					'name'  =>  esc_html_x('Sidebar Position','meta boxes','daylight'),
					'type'  =>  'select',
					'values'    =>  array(
							'as_blog'   	=>  esc_html_x('Same as Blog Page','meta boxes','daylight'),
							'full_width'    =>  esc_html_x('No Sidebar/Full Width','meta boxes','daylight'),
							'right'         =>  esc_html_x('Right','meta boxes','daylight'),
							'left'          =>  esc_html_x('Left','meta boxes','daylight'),
						),
					'std'   =>  'right'  //default value selected
				),

				'header_type'=>array(
					'name'=> esc_html_x('Header Type','meta boxes','daylight'),
					'type'=>'select',
					'values'=>array(
							'sticky'=>esc_html_x('Sticky','meta boxes','daylight'),
							'no-sticky'=>esc_html_x('No Sticky','meta boxes','daylight'),
					),
				'std'=> $menu_style  ? $menu_style : (_go('header_style') ? _go('header_style') : 'no-sticky')
				),

				'page_background'=>array(
		    		'name'=>'Hero Background',
		    		'type'=>'image',
		    		'desc'=>'If you want a different background to be used for this page, then you can upload it here.'
		    	),


		    	'hide_hero'=>array(
		    		'name' => 'Hide hero-section?',
		    		'type' => 'checkbox',
		    		'desc'=> "Check if you want to hide hero-section, usually it's used when you create page with Visual Composer" ,
	    		),
			)
		),
	)
);