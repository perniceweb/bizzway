<div>
	<?php if(!empty($label)) : ?>
		<div class="input-cover"><?php echo $label ?></div>
	<?php endif; ?>
	<select
		class="input-line js-input"
		name="<?php echo esc_attr($name)?>"
		<?php if($required) echo 'data-parsley-required="true" data-trigger="change"'; ?>
		>
		<?php 
		$options = explode(',', $select_options);
		if(isset($placeholder)) echo '<option value="">'.$placeholder.'</option>';
		if(!empty($options)) :
			foreach ($options as $value) : ?>
				<option value="<?php echo $value?>"><?php echo $value ?></option>
			<?php endforeach ?>
		<?php else: ?>
			<option value=""><?php _e('No Options Inserted','magellan') ?></option>
		<?php endif; ?>
	</select>
</div>