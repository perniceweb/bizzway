<input 
	type="submit"
	class="btn form-submit purple" 
 	data-sending='<?php esc_html_e('Sending Message','daylight') ?>'
	data-sent='<?php esc_html_e('Message Successfully Sent','daylight') ?>'
	value="<?php print isset($label) && $label !== '' ? esc_attr($label) : esc_html__('Write','daylight'); ?>">