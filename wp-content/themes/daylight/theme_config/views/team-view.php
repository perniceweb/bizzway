<?php
$nr = $shortcode['nr'];
$columns = $shortcode['columns'] ? explode("|", $shortcode['columns']) : array('col-sm-6','2');
$use_slider = $shortcode['use_slider'];
$el_class = $shortcode['el_class'];
$css_class = !empty($shortcode['css']) ? vc_shortcode_custom_css_class( $shortcode['css'] ) : '';

if(!empty($shortcode['offset']))
	$slides = array_slice($slides,$shortcode['offset']);
?>

<div class="team-feed <?php echo esc_attr($css_class.' '.$el_class);?>">
<?php if($use_slider):?>
<div class="slick-carousel team-members" data-speed="800" data-dots="false" data-infinite="true" data-items-desktop="<?php echo esc_attr($columns[1]);?>" data-items-tablet="3" data-items-mobile="2" data-items-xs-mobile="1">
	<div class="container">
<?php endif;?>	
		<div class="row carousel-items">
			<?php foreach ($slides as $slide_nr => $slide) : if($slide_nr >= $shortcode['nr']) break; 
			$small_image = $slide['options']['image'];
			?>
			<div class="<?php echo esc_attr($columns[0]);?>">
				<div class="team-member">
					<div class="cover">
						<img src="<?php echo esc_url( $small_image->url );?>" alt="<?php echo get_the_title($slide['post']->ID);?>">

						<ul class="social-links">
							<?php foreach ($slide['options']['social_links'] as $network): ?>
							<li><a href="<?php echo esc_url($network['url']);?>"><i class="fa fa-<?php echo esc_attr($network['title']);?>"></i></a></li>
							<?php endforeach; ?>
						</ul>
					</div>

					<h3 class="name"><?php echo get_the_title($slide['post']->ID);?></h3>
					<p class="position"><?php print $slide['options']['position'];?></p>
				</div>
			</div>
			<?php endforeach;?>
		</div>
<?php if($use_slider):?>
	</div>
</div>
<?php endif;?>
</div>