<?php
$nr = $shortcode['nr'];
$category = $shortcode['item_category'] ? '.'.$shortcode['item_category'] : '*';
$column = $shortcode['columns'] ? $shortcode['columns'] : 'col-xs-6 col-md-4';
$filters = $shortcode['hide_filters'];
$el_class = $shortcode['el_class'];
$css_class = !empty($shortcode['css']) ? vc_shortcode_custom_css_class( $shortcode['css'] ) : '';

if(!empty($shortcode['offset']))
	$slides = array_slice($slides,$shortcode['offset']);
?>
<div class="portfolio-wrapper <?php echo esc_attr($css_class.' '.$el_class);?>">
	<?php if(!$filters):?>
	<div class="portfolio-filters isotope-filters">
		<ul class="clean-list">
			<li><a data-filter="*" href="#" <?php if($category == '*') echo 'class="current"';?>><?php esc_html_e('All','daylight');?></a></li>
			<?php foreach($all_categories as $category_slug => $category_name): ?>
	            <li><a href="#" data-filter=".<?php echo esc_attr($category_slug); ?>" <?php if('.'.$category_slug==$category) echo 'class="current"';?>><?php print $category_name; ?></a></li>
	        <?php endforeach; ?>
		</ul>
	</div>
	<?php endif;?>

	<div class="row isotope-container" data-default-selection="<?php echo esc_attr($category);?>">
		<?php foreach ($slides as $slide_nr => $slide) : if($slide_nr >= $shortcode['nr']) break; 
		$small_image = $slide['options']['cover_image'];
		?>
		<div class="<?php echo esc_attr($column);?> isotope-item <?php echo implode(' ', array_keys($slide['categories'])); ?>">
			<div class="project-item">
				<a href="<?php echo get_the_permalink($slide['post']->ID);?>">
					<img src="<?php echo esc_url( $small_image->url );?>" alt="<?php echo get_the_title($slide['post']->ID);?>">
				</a>

				<h3 class="cover-title"><?php echo get_the_title($slide['post']->ID);?></h3>
			</div>
		</div>
		<?php endforeach;?>
	</div>
</div>