<?php 
$el_class = $shortcode['el_class'];
$columns = $shortcode['columns'] ? explode("|", $shortcode['columns']) : array(2,2);
$css_class = !empty($shortcode['css']) ? vc_shortcode_custom_css_class( $shortcode['css'] ) : '';
if(!empty($shortcode['offset']))
    $slides = array_slice($slides,$shortcode['offset']);
?>

<div class="slick-carousel testimonials-carousel <?php echo esc_attr($css_class.' '.$el_class);?>" data-speed="800" data-dots="false" data-infinite="true" data-items-desktop="<?php echo esc_attr($columns[0]);?>" data-items-tablet="<?php echo esc_attr($columns[1]);?>" data-items-mobile="2" data-items-xs-mobile="1">
    <div class="container">
        <div class="row carousel-items">
            <?php foreach ($slides as $slide_nr => $slide) : if($slide_nr >= $shortcode['nr']) break;
            $thumbnail_id = wp_get_attachment_image_src(get_post_thumbnail_id($slide['post']->ID), array(81,81) );
            ?>
            <div class="col-md-4 col-sm-6">
                <div class="testimonial">
                    <div class="message">
                        <i class="icon icon-high-volume"></i>
                        <p><?php echo get_post_field('post_content',$slide['post']->ID);?></p>
                    </div>

                    <?php if(!empty($thumbnail_id)):?>
                    <img src="<?php echo esc_url($thumbnail_id[0]);?>" alt="<?php echo get_the_title($slide['post']->ID);?>">
                    <?php endif;?>

                    <h3 class="name"><?php echo get_the_title($slide['post']->ID);?></h3>
                    <span class="position"><?php print $slide['options']['position'];?></span>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>