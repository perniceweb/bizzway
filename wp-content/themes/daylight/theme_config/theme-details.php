<?php
define('THEME_NAME', 'daylight');
define('THEME_PRETTY_NAME', 'Daylight');

//Load Textdomain
add_action('after_setup_theme', 'tt_theme_textdomain_setup');
function tt_theme_textdomain_setup(){
	if(load_theme_textdomain('daylight', get_template_directory() . '/languages'))
		define('TT_TEXTDOMAIN_LOADED',true);
}

//content width
if (!isset($content_width))
    $content_width = 1170;

//============Theme support=======
//post-thumbnails
add_theme_support('post-thumbnails');

function tt_add_thumb_sizes() {
    add_image_size( 'featured-image', 720, 300, true );
}

add_action( 'after_setup_theme', 'tt_add_thumb_sizes' );
//add feed support
add_theme_support('automatic-feed-links');
//add  html5 support
add_theme_support('html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'));
//add  postformats support
add_theme_support('post-formats', array('gallery','video','audio','image'));