<?php get_header(); ?>	

<?php
/**
 * 404 Page
 */
?>
<section class="error-404">
	<div class="container">	
		<a class="return-btn" href="<?php echo home_url('/');?>"><?php esc_html_e('Back','daylight');?></a>

		<h2 class="title"><?php print _go('error_title') ? _go('error_title') : esc_html__('Error 404', 'daylight'); ?></h2>
		<h3 class="subtitle"><?php print _go('error_subtitle');?></h3>

		<i class="icon icon-garbage"></i>
	</div>
</section>

<?php get_footer(); ?>